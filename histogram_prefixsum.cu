/* Udacity Homework 3 */

#include "utils.h"

__global__ void
max_reduce(float *out, const float *in, size_t size) {
	extern __shared__ float buf[];
	
	int tid = threadIdx.x;
	int bid = blockIdx.x;
	int gid = threadIdx.x + blockIdx.x * blockDim.x;
	
	if (gid < size) {
		buf[tid] = in[gid];
	} else {
		buf[tid] = in[size - 1];
	}
	__syncthreads();

	float f, s;
	for (int i = blockDim.x / 2; i; i /= 2) {
		if (tid < i) {
			__syncthreads();
			f = buf[tid], s = buf[tid + i];
			__syncthreads();
			buf[tid] = max(f, s);
		}
	}
	__syncthreads();
	out[bid] = buf[0];
}

__global__ void
min_reduce(float *out, const float *in, size_t size) {
	extern __shared__ float buf[];
	
	int tid = threadIdx.x;
	int bid = blockIdx.x;
	int gid = threadIdx.x + blockIdx.x * blockDim.x;

	float *out_min = out;
	float *mmin = buf;
	
	if (gid < size) {
		mmin[tid] = in[gid];
	} else {
		mmin[tid] = in[size - 1];
	}
	__syncthreads();

	float f, s;
	for (int i = blockDim.x / 2; i; i /= 2) {
		if (tid < i) {
			__syncthreads();
			f = mmin[tid], s = mmin[tid + i];
			__syncthreads();
			mmin[tid] = min(f, s);
		}
	}
	__syncthreads();
	out_min[bid] = mmin[0];
}

__global__ void
histogram_atomic(unsigned int *out, const float *in, size_t size, float lumMin, float range, int numBins) {
	extern __shared__ int local_hist[];
	
	int tid = threadIdx.x;
	int gid = threadIdx.x + blockIdx.x * blockDim.x;
	
	if (gid < size) {
		unsigned int bin = (in[gid] - lumMin) / range * numBins;
		local_hist[tid] = bin;
	} else {
		local_hist[tid] = -1;
	}
	__syncthreads();
	
	unsigned int count = 0;
	for (int i = 0; i < blockDim.x; i++) {
		if (local_hist[i] == tid) {
			count++;
		}
	}
	atomicAdd(&out[tid], count);
}

__global__ void
exclusive_sum_scan(unsigned int *out, const unsigned int *in) {
	unsigned int *ssbuf = out;
	int tid = threadIdx.x;
	unsigned int l, r;
	int j, i;
	
	ssbuf[tid] = in[tid];
	for (i = blockDim.x/2, j = 1; i; i /= 2, j *= 2) {
		__syncthreads();
		if (tid < i) {
			l = ssbuf[blockDim.x - 1 - tid * 2 * j - j],
			r = ssbuf[blockDim.x - 1 - tid * 2 * j];
		}
		__syncthreads();
		if (tid < i) {
			ssbuf[blockDim.x - 1 - tid * 2 * j] = l + r;
		}
	}
	ssbuf[blockDim.x - 1] = 0;
	for (j = blockDim.x/2, i = 1; j; j /= 2, i *= 2) {
		__syncthreads();
		if (tid < i) {
			l = ssbuf[blockDim.x - 1 - tid * 2 * j - j],
			r = ssbuf[blockDim.x - 1 - tid * 2 * j];
		}
		__syncthreads();
		if (tid < i) {
			ssbuf[blockDim.x - 1 - tid * 2 * j] = l + r;
			ssbuf[blockDim.x - 1 - tid * 2 * j - j] = r;
		}
	}
	__syncthreads();
}

void your_histogram_and_prefixsum(const float* const d_logLuminance,
                                  unsigned int* const d_cdf,
                                  float &min_logLum,
                                  float &max_logLum,
                                  const size_t numRows,
                                  const size_t numCols,
                                  const size_t numBins)
{
	float *d_min, *d_max, *d_min_res, *d_max_res;
	float range;
	unsigned int *d_histo;
	float h_min, h_max;
	size_t size = numRows * numCols;
	size_t nth;
	size_t nbl;
	size_t shmem;
	
	
	nth = 1024;
	nbl = size / nth + 1;
	checkCudaErrors(cudaMalloc(&d_min, sizeof(*d_min) * nbl));
	checkCudaErrors(cudaMalloc(&d_max, sizeof(*d_max) * nbl));
	checkCudaErrors(cudaMalloc(&d_min_res, sizeof(*d_min_res)));
	checkCudaErrors(cudaMalloc(&d_max_res, sizeof(*d_max_res)));
	
	shmem = nth * sizeof(float);
	min_reduce<<<nbl, nth, shmem>>>(d_min, d_logLuminance, size);
	max_reduce<<<nbl, nth, shmem>>>(d_max, d_logLuminance, size);
	
	nth = nbl + nbl%2;
	shmem = nth * sizeof(float);
	nbl = 1;
	min_reduce<<<nbl, nth, shmem>>>(d_min_res, d_min, nth);
	max_reduce<<<nbl, nth, shmem>>>(d_max_res, d_max, nth);
	
	cudaMemcpy(&h_min, d_min_res, sizeof(*d_min_res), cudaMemcpyDeviceToHost);
	cudaMemcpy(&h_max, d_max_res, sizeof(*d_min_res), cudaMemcpyDeviceToHost);
	
	checkCudaErrors(cudaFree(d_min));
	checkCudaErrors(cudaFree(d_max));
	checkCudaErrors(cudaFree(d_min_res));
	checkCudaErrors(cudaFree(d_max_res));
	
	min_logLum = h_min;
	max_logLum = h_max;
	
	checkCudaErrors(cudaMalloc(&d_histo, sizeof(*d_histo) * numBins));
	cudaMemset(d_histo, 0, sizeof(*d_histo) * numBins);
	
	range = max_logLum - min_logLum;
	nbl = size / numBins + 1;
	nth = numBins;
	shmem = nth * sizeof(unsigned int);
	histogram_atomic<<<nbl, nth, shmem>>>(d_histo, d_logLuminance, size, min_logLum, range, numBins);

	nbl = 1;
	nth = numBins;
	shmem = numBins * sizeof(unsigned int);
	exclusive_sum_scan<<<nbl, nth, shmem>>>(d_cdf, d_histo);
	checkCudaErrors(cudaFree(d_histo));
}