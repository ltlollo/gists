#define VK_USE_PLATFORM_XCB_KHR
#include <vulkan/vulkan.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <unistd.h>
#include <err.h>
#include <dlfcn.h>

#define xensure(cond) do {\
    if ((cond) == 0) errx(1, "%s", "invariant " #cond " violated"); \
} while (0)
#define cxsize(x) (sizeof((x)) / sizeof((*x)))

#define xvkerr(f) do {\
    xensure((f) == VK_SUCCESS);\
} while (0)

const uint32_t *
vertex_shader(size_t *size) {
    extern const char _binary_vert_spv_start;
    extern const char _binary_vert_spv_end;

    *size = &_binary_vert_spv_end - &_binary_vert_spv_start;

    return (const uint32_t *)&_binary_vert_spv_start;
}

const uint32_t *
fragment_shader(size_t *size) {
    extern const char _binary_frag_spv_start;
    extern const char _binary_frag_spv_end;

    *size = &_binary_frag_spv_end - &_binary_frag_spv_start;

    return (const uint32_t *)&_binary_frag_spv_start;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL
debug_callback(
    VkDebugReportFlagsEXT flags,
    VkDebugReportObjectTypeEXT objType,
    uint64_t obj,
    size_t location,
    int32_t code,
    const char* layerPrefix,
    const char* msg,
    void* userData) {

    fprintf(stderr, "validation layer: %s\n", msg);;

    return VK_FALSE;
}

int
main() {
    VkDevice device;
    VkPhysicalDevice gpu;
    VkSwapchainKHR swapchain;
    VkInstance inst;
    VkSurfaceKHR surface;
    VkShaderModule vert_module;
    VkShaderModule frag_module;
    VkPipelineLayout pipeline_layout;
    VkRenderPass renderpass;
    VkPipeline pipeline;
    VkImage images[2];
    VkImageView image_views[2];
    VkFramebuffer framebuffers[2];
    VkSemaphore image_sem;
    VkSemaphore render_sem;
    VkQueue graphics_queue;
    VkDebugReportCallbackEXT callback;

    static const char *app_exts[] = {
        VK_KHR_SURFACE_EXTENSION_NAME,
        VK_KHR_XCB_SURFACE_EXTENSION_NAME,
        VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
    };
    uint32_t app_exts_count = cxsize(app_exts);
    static const char *dev_exts[] = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    };
    uint32_t dev_exts_count = cxsize(dev_exts);
    static const char *layers[] = { "VK_LAYER_LUNARG_standard_validation" };
    uint32_t layer_count = cxsize(layers);

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
    SDL_Window *win = SDL_CreateWindow("foo", 0, 0, 800, 600
        , SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_MAXIMIZED
        | SDL_WINDOW_RESIZABLE
    );

    VkApplicationInfo app_info = {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pNext = NULL,
        .pApplicationName = "foo",
        .applicationVersion = 0,
        .pEngineName = "foo",
        .engineVersion = 0,
        .apiVersion = VK_API_VERSION_1_1
    };
    if (!SDL_Vulkan_GetInstanceExtensions(win, &app_exts_count, app_exts)) {
        errx(1, "%s", SDL_GetError());
    }

    VkInstanceCreateInfo inst_info = {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pNext = NULL,
        .pApplicationInfo = &app_info,
        .enabledLayerCount = layer_count,
        .ppEnabledLayerNames = layers,
        .enabledExtensionCount = app_exts_count,
        .ppEnabledExtensionNames = app_exts
    };
    xvkerr(vkCreateInstance(&inst_info, NULL, &inst));

    VkDebugReportCallbackCreateInfoEXT debug_info = {
        .sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT,
        .flags = VK_DEBUG_REPORT_ERROR_BIT_EXT
            | VK_DEBUG_REPORT_WARNING_BIT_EXT,
        .pfnCallback = debug_callback,
    };
    PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT =
        (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(inst,
            "vkCreateDebugReportCallbackEXT"
    );
    if (vkCreateDebugReportCallbackEXT != NULL) {
        xvkerr(vkCreateDebugReportCallbackEXT(inst, &debug_info, NULL,
                &callback
        ));
    } else {
        fprintf(stderr, "Sorry, no debug symbols for you\n");
    }
    if (!SDL_Vulkan_CreateSurface(win, inst, &surface)) {
        errx(1, "%s", SDL_GetError());
    }
    uint32_t gpu_count = 1;
    xvkerr(vkEnumeratePhysicalDevices(inst, &gpu_count, &gpu));

    uint32_t queue_count;
    vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queue_count, NULL);
    VkQueueFamilyProperties queue_props[queue_count];
    vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queue_count, queue_props);

    VkPhysicalDeviceFeatures features;
    vkGetPhysicalDeviceFeatures(gpu, &features);

    uint32_t queue_graphics_idx = UINT32_MAX;
    for (int i = 0; i < queue_count; i++) {
        if ((queue_props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0) {
            queue_graphics_idx = i;
        }
    }
    float queue_priorities = .0;

    VkDeviceQueueCreateInfo queue_info = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .queueFamilyIndex = queue_graphics_idx,
        .queueCount = 1,
        .pQueuePriorities = &queue_priorities
    };
    VkDeviceCreateInfo device_info = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .queueCreateInfoCount = 1,
        .pQueueCreateInfos = &queue_info,
        .enabledExtensionCount = dev_exts_count,
        .ppEnabledExtensionNames = dev_exts,
        .enabledLayerCount = layer_count,
        .ppEnabledLayerNames = layers,
    };
    xvkerr(vkCreateDevice(gpu, &device_info, NULL, &device));

    VkBool32 supports_present;
    vkGetPhysicalDeviceSurfaceSupportKHR(gpu, queue_graphics_idx, surface,
        &supports_present
    );
    xensure(supports_present);

    VkSurfaceCapabilitiesKHR surface_cap;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(gpu, surface, &surface_cap);

    uint32_t surface_format_count = 1;
    VkSurfaceFormatKHR surface_format;
    vkGetPhysicalDeviceSurfaceFormatsKHR(gpu, surface, &surface_format_count,
        &surface_format
    );
    xensure(surface_format_count);
    if (surface_format.format == VK_FORMAT_UNDEFINED) {
        surface_format.format = VK_FORMAT_B8G8R8A8_UNORM;
        surface_format.colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
    }
    VkSwapchainCreateInfoKHR swapchain_info = {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .surface = surface,
        .minImageCount = 2,
        .imageFormat = surface_format.format,
        .imageColorSpace = surface_format.colorSpace,
        .imageExtent = surface_cap.currentExtent,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT
            | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
        .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = NULL,
        .preTransform = surface_cap.currentTransform,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = VK_PRESENT_MODE_FIFO_KHR,
        .clipped = VK_TRUE,
        .oldSwapchain = NULL,
    };
    xvkerr(vkCreateSwapchainKHR(device, &swapchain_info, NULL, &swapchain));

    uint32_t swapchain_image_count = 2;

    xvkerr(vkGetSwapchainImagesKHR(device, swapchain, &swapchain_image_count,
            images
    ));

    for (int i = 0; i < swapchain_image_count; i++) {
        VkImageViewCreateInfo image_view_info = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .image = images[i],
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = surface_format.format,
            .components = {
                .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                .a = VK_COMPONENT_SWIZZLE_IDENTITY,
            },
            .subresourceRange = {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
        };
        xvkerr(vkCreateImageView(device, &image_view_info, NULL,
                image_views + i
        ));
    }

    size_t vertex_size;
    const uint32_t *vertex_code = vertex_shader(&vertex_size);
    VkShaderModuleCreateInfo vert_shader_info = {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .codeSize = vertex_size,
        .pCode = vertex_code
    };
    xvkerr(vkCreateShaderModule(device, &vert_shader_info, NULL,
            &vert_module
    ));

    size_t fragment_size;
    const uint32_t *fragment_code = fragment_shader(&fragment_size);
    VkShaderModuleCreateInfo frag_shader_info = {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .codeSize = fragment_size,
        .pCode = fragment_code
    };
    xvkerr(vkCreateShaderModule(device, &frag_shader_info, NULL,
            &frag_module
    ));

    VkPipelineShaderStageCreateInfo shader_pipeline_info[] = {
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = VK_SHADER_STAGE_VERTEX_BIT,
            .module = vert_module,
            .pName = "main",    
        }, {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
            .module = frag_module,
            .pName = "main",
        }
    };
    VkPipelineVertexInputStateCreateInfo vertex_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 0,
        .vertexAttributeDescriptionCount = 0,
    };
    VkPipelineInputAssemblyStateCreateInfo input_assembly = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };
    VkViewport viewport = {
        .x = 0.0,
        .y = 0.0,
        .width = swapchain_info.imageExtent.width,
        .height = swapchain_info.imageExtent.height,
        .maxDepth = 1.0,
        .minDepth = 1.0,
    };
    VkRect2D scissor = {
        .offset = { 0, 0 },
        .extent = swapchain_info.imageExtent
    };
    VkPipelineViewportStateCreateInfo viewport_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .pViewports = &viewport,
        .scissorCount = 1,
        .pScissors = &scissor,
    };
    VkPipelineRasterizationStateCreateInfo rasterizer_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .lineWidth = 1.0,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
    };
    VkPipelineMultisampleStateCreateInfo multisample_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .sampleShadingEnable = VK_FALSE,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
    };
    VkPipelineColorBlendAttachmentState blend_attach = {
        .colorWriteMask =  VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT
            | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
        .blendEnable = VK_FALSE
    };
    VkPipelineColorBlendStateCreateInfo blend_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .attachmentCount = 1,
        .pAttachments = &blend_attach
    };
    VkDynamicState dynamic_states[] = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_LINE_WIDTH
    };
    VkPipelineDynamicStateCreateInfo dynstate_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = cxsize(dynamic_states),
        .pDynamicStates = dynamic_states,
    };

    VkPipelineLayoutCreateInfo pipeline_layout_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
    };
    xvkerr(vkCreatePipelineLayout(device, &pipeline_layout_info, NULL,
            &pipeline_layout
    ));

    VkAttachmentDescription color_attachment = {
        .format = swapchain_info.imageFormat,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    };
    VkAttachmentReference attach_ref = {
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };
    VkSubpassDescription subpass = {
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = 1,
        .pColorAttachments = &attach_ref,
    };
    VkSubpassDependency subpass_dep = {
        .srcSubpass = VK_SUBPASS_EXTERNAL,
        .dstSubpass = 0,
        .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .srcAccessMask = 0,
        .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
            VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
    };
    VkRenderPassCreateInfo renderpass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = 1,
        .pAttachments = &color_attachment,
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = 1,
        .pDependencies = &subpass_dep,
    };
    xvkerr(vkCreateRenderPass(device, &renderpass_info, NULL, &renderpass));

    VkGraphicsPipelineCreateInfo graphics_info = {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = 2,
        .pStages = shader_pipeline_info,
        .pVertexInputState = &vertex_info,
        .pInputAssemblyState = &input_assembly,
        .pViewportState = &viewport_info,
        .pRasterizationState = &rasterizer_info,
        .pMultisampleState = &multisample_info,
        .pColorBlendState = &blend_info,
        .pDynamicState = &dynstate_info,
        .layout = pipeline_layout,
        .renderPass = renderpass,
        .subpass = 0,
    };
    xvkerr(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &graphics_info,
            NULL, &pipeline
    ));

    for (int i = 0; i < cxsize(framebuffers); i++) {
        VkFramebufferCreateInfo frambebuffer_info = {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = renderpass,
            .attachmentCount = 1,
            .pAttachments = image_views + i,
            .width = swapchain_info.imageExtent.width,
            .height = swapchain_info.imageExtent.height,
            .layers = 1,
        };
        xvkerr(vkCreateFramebuffer(device, &frambebuffer_info, NULL,
                framebuffers + i
        ));
    }

    VkCommandPool command_pool;
    VkCommandPoolCreateInfo pool_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .queueFamilyIndex = queue_graphics_idx,
    };
    xvkerr(vkCreateCommandPool(device, &pool_info, NULL, &command_pool));

    VkCommandBuffer command_buffers[2];
    VkCommandBufferAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = command_pool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = cxsize(command_buffers),
    };
    xvkerr(vkAllocateCommandBuffers(device, &alloc_info, command_buffers));

    VkClearValue clear_color = {
        .color.float32 = { 0., 0., 0., 1., },
    };
    for (int i = 0; i < cxsize(command_buffers); i++) {
        VkCommandBufferBeginInfo begin_info = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
        };
        vkBeginCommandBuffer(command_buffers[i], &begin_info);

        VkRenderPassBeginInfo renderpass_info = {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            .renderPass = renderpass,
            .framebuffer = framebuffers[i],
            .renderArea = {
                .offset = { 0, 0 },
                .extent = swapchain_info.imageExtent,
            },
            .clearValueCount = 1,
            .pClearValues = &clear_color,
        };
        vkCmdBeginRenderPass(command_buffers[i], &renderpass_info,
            VK_SUBPASS_CONTENTS_INLINE
        );
        vkCmdBindPipeline(command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipeline
        );
        vkCmdSetViewport(command_buffers[i], 0, 1, &viewport);
        vkCmdDraw(command_buffers[i], 3, 1, 0, 0);
        vkCmdEndRenderPass(command_buffers[i]);

        xvkerr(vkEndCommandBuffer(command_buffers[i]));
    }

    VkSemaphoreCreateInfo sem_info = {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
    };
    xvkerr(vkCreateSemaphore(device, &sem_info, NULL, &image_sem));
    xvkerr(vkCreateSemaphore(device, &sem_info, NULL, &render_sem));
    vkGetDeviceQueue(device, queue_graphics_idx, 0, &graphics_queue);

    VkPipelineStageFlags stages = {
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
    };
    uint32_t image_idx;
    VkSubmitInfo submit_info = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &image_sem,
        .pWaitDstStageMask = &stages,
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = &render_sem,
        .commandBufferCount = 1,
    };
    VkPresentInfoKHR present_info = {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &render_sem,
        .swapchainCount = 1,
        .pSwapchains = &swapchain,
        .pImageIndices = &image_idx,
    };

    SDL_Event ev;
    while (SDL_WaitEvent(&ev)) {
        if (ev.key.keysym.sym == 'q') {
            exit(0);
        }
        vkAcquireNextImageKHR(device, swapchain, ~0ull, image_sem,
            VK_NULL_HANDLE, &image_idx
        );
        submit_info.pCommandBuffers = command_buffers + image_idx;
        xvkerr(vkQueueSubmit(graphics_queue, 1, &submit_info, VK_NULL_HANDLE));
        vkQueuePresentKHR(graphics_queue, &present_info);
    }
}
