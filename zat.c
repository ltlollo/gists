// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <err.h>

#ifdef DEBUG
#   define DBG(x) x
#else
#   define DBG(x)
#endif

extern char *__progname;
typedef unsigned char u8;
typedef size_t u32f;
typedef unsigned u32;

struct problem {
    u8 sign;
    u32 maxp;
    u32 nclause;
    u32 nvars;
    u8 *mem;
    u8 *data;
    u32 *pos;
    u32 *neg;
    u32 *clc;
    u32 *cln;
};

struct errcnf {
    u32 l;
    u32 c;
    const char *e;
};

static int verify(struct problem *, char *);
static void mkerr(struct errcnf *, u32, u32, const char *);
static int readcnf(const char *, struct problem *, struct errcnf *);
static u8 masksum(u8 *restrict, u8 *restrict, u32f);
static void decrpos(u8 *, u32 *, u32f);
static void decrneg(u8 *, u32 *, u32f);
static u32 popcnt(u8 *, u32f);
static int choose(struct problem *, u32, u8, char *, struct problem *);
static int reduce(struct problem *, char *, u8 *);
static int unitprop(struct problem *, char *);
static int verify(struct problem *, char *);
static void __attribute__((unused)) showprob(struct problem *, char *);
static int __attribute__((unused)) chkinvar(struct problem *);
static int __attribute__((unused)) solve(struct problem *, char *, u8 *);
static int __attribute__((unused)) nrsolve(struct problem *, char *);

static const u32 v4[][4] = {
    [0x01] = {  [0] = 1,    [1] = 0,    [2] = 0,    [3] = 0,    },
    [0x04] = {  [0] = 0,    [1] = 1,    [2] = 0,    [3] = 0,    },
    [0x05] = {  [0] = 1,    [1] = 1,    [2] = 0,    [3] = 0,    },
    [0x10] = {  [0] = 0,    [1] = 0,    [2] = 1,    [3] = 0,    },
    [0x11] = {  [0] = 1,    [1] = 0,    [2] = 1,    [3] = 0,    },
    [0x14] = {  [0] = 0,    [1] = 1,    [2] = 1,    [3] = 0,    },
    [0x15] = {  [0] = 1,    [1] = 1,    [2] = 1,    [3] = 0,    },
    [0x40] = {  [0] = 0,    [1] = 0,    [2] = 0,    [3] = 1,    },
    [0x41] = {  [0] = 1,    [1] = 0,    [2] = 0,    [3] = 1,    },
    [0x44] = {  [0] = 0,    [1] = 1,    [2] = 0,    [3] = 1,    },
    [0x45] = {  [0] = 1,    [1] = 1,    [2] = 0,    [3] = 1,    },
    [0x50] = {  [0] = 0,    [1] = 0,    [2] = 1,    [3] = 1,    },
    [0x51] = {  [0] = 1,    [1] = 0,    [2] = 1,    [3] = 1,    },
    [0x54] = {  [0] = 0,    [1] = 1,    [2] = 1,    [3] = 1,    },
    [0x55] = {  [0] = 1,    [1] = 1,    [2] = 1,    [3] = 1,    },
};

int
main(int argc, char *argv[]) {
    if (argc - 1 != 1) {
        errx(1, "not enough arguments"
            "\nUsage: %s file"
            "\n\tfile<string>: a file in a cnf format"
            "\nScope: solve a cnf equation specified in a cnf"
            , __progname
        );
        return 1;
    }
    const char *fname = argv[1];
    struct problem p;
    struct errcnf e;
    if (readcnf(fname, &p, &e) != 0) {
        if (e.l) {
            errx(1, "%s:%u:%u: %s", fname, e.l, e.c, e.e);
        } else {
            err(1, "readcnf");
        }
    }
    char *sol = calloc(p.nvars, 1);

    if (!sol) {
        err(1, "calloc");
    }
    int res = nrsolve(&p, sol);

    switch (res) {
        case -1:
            err(1, "solve");
        case +1:
            printf("S: Unsat\n");
            return 0;
        case +0:
            if ((res = verify(&p, sol)) != 0) {
                warnx("bogus solution");
                DBG(break);
                return -res;
            }
            printf("S: ");
            for (u32f i = 0; i < p.nvars; i++) {
                if (sol[i]) {
                    printf("% 2ld ", sol[i] * (i + 1));
                }
            }
            puts("");
            return 0;
    }
    return -res;
}

static int
readcnf(const char *fname, struct problem *p, struct errcnf *e) {
    int err = 1;
    size_t lsize = 256, alloc;
    FILE *f;
    char *line;
    ssize_t rsize;
    long nvars, nclause, val;
    char *snum, *end;
    u32f pad_nvars;
    u32f pad_sign;
    u32f l = 0;

    e->l = e->c = 0;
    p->maxp = 0;
    p->sign = 0;

    if (!(f = fopen(fname, "r"))) {
        goto ERR_FOPEN;
    }
    if (!(line = malloc(lsize))) {
        goto ERR_LINE;
    }
    while (1) {
        l++;
        if ((rsize = getline(&line, &lsize, f)) == -1) {
            mkerr(e, l, 1, "missing problem directive");
            goto ERR_PARSE;
        }
        if (line[0] == 'c' && line[1] == ' ') {
            continue;
        } else if (line[0] == 'p') {
            end = line + 1;
            while (*end == ' ' || *end == '\t') end++;
            if (strncmp(end, "cnf", 3) != 0) {
                mkerr(e, l, 2, "expecting cnf");
                goto ERR_PARSE;
            }
            end = end + 3;
            while (*end == ' ' || *end == '\t') end++;
            if ((nvars = strtol((snum = end), &end, 10)), end == snum) {
                mkerr(e, l, snum - line, "parsing #vars");
                goto ERR_PARSE;
            }
            if (nvars  < 0 || nvars > UINT_MAX - 7) {
                mkerr(e, l, snum - line, "#vars out of range");
                goto ERR_PARSE;
            }
            while (*end == ' ' || *end == '\t') end++;
            if ((nclause = strtol((snum = end), &end, 10)), end == snum) {
                mkerr(e, l, snum - line, "parsing #clause");
                goto ERR_PARSE;
            }
            if (nclause < 0 || nclause > UINT_MAX) {
                mkerr(e, l, snum - line, "#clause out of range");
                goto ERR_PARSE;
            }
            break;
        } else {
            mkerr(e, l, 1, "unknown directive");
            goto ERR_PARSE;
        }
    }
    pad_nvars = (nvars * 2 + 7) / 8;
    pad_sign = ((nvars + 3) & -4);
    alloc = nclause * pad_nvars + sizeof(u32) * ( 2 * pad_sign + 2 * nclause);
    if (!(p->mem = calloc(alloc, 1))) {
        goto ERR_LINE;
    }
    p->nvars = nvars;
    p->nclause = nclause;
    p->pos = (u32 *)p->mem;
    p->neg = p->pos + pad_sign;
    p->clc = p->neg + pad_sign;
    p->cln = p->clc + nclause;
    p->data = (u8 *)(p->cln + nclause);

    for (u32 i = 0; i < nclause; i++) {
        p->cln[i] = i;
    }
    u32 clause = 0;
    u8 *data;

    while (1) {
        l++;
        data = p->data + pad_nvars * clause;
        if ((rsize = getline(&line, &lsize, f)) == -1) {
            if (nclause == 0) {
                err = 0;
            }
            mkerr(e, l, 1, "expected remaining clauses");
            goto ERR_CHOOSE;
        }
        if (line[0] == 'c' && line[1] == ' ') {
            continue;
        } else if (clause == p->nclause) {
            mkerr(e, l, 1, "unexpected clause");
            goto ERR_CHOOSE;
        }
        end = line;
        while (1) {
            while (*end == ' ' || *end == '\t') end++;
            snum = end;
            if ((val = strtol((end = snum), &end, 10)), end == snum) {
                mkerr(e, l, end - line, "parsing variable");
                goto ERR_LINE;
            }
            if (val < -nvars || val > nvars) {
                mkerr(e, l, end - line, "variable out of range");
                goto ERR_LINE;
            }
            if (val > 0) {
                val = val - 1;
                p->pos[val]++;
                val = 2 * val;
                data[val / 8] |= 1 << (val % 8);
            } else if (val < 0) {
                val = -val - 1;
                p->neg[val]++;
                val = 2 * val;
                data[val / 8] |= 2 << (val % 8);
            } else {
                break;
            }
        }
        while (*end == ' ' || *end == '\t') end++;
        if (*end != '\0' && *end != '\n') {
            mkerr(e, l, end - line, "garbage at the end of line");
            goto ERR_CHOOSE;
        }
        if (++clause == p->nclause) {
            break;
        }
    }
    if (clause != p->nclause) {
        mkerr(e, l, end - line, "expected remaining clauses");
        goto ERR_CHOOSE;
    }
    while (1) {
        l++;
        if ((rsize = getline(&line, &lsize, f)) == -1) {
            break;
        }
        if (line[0] == 'c' && line[1] == ' ') {
            continue;
        }
        mkerr(e, l, 1, "garbage at the end of file");
        goto ERR_CHOOSE;
    }
    for (u32f i = 0; i < p->nclause; i++) {
        p->clc[i] = popcnt(p->data + i * pad_nvars, pad_nvars);
    }
    err = 0;
ERR_CHOOSE:
    if (err) {
        free(p->mem);
    }
ERR_LINE:
    free(line);
ERR_PARSE:
    fclose(f);
ERR_FOPEN:
    return -err;
}

static int __attribute__((unused))
solve(struct problem *p, char *sol, u8 *smask) {
    int res;
    u32f pad_nvars = (p->nvars * 2 + 7) / 8;

    DBG(showprob(p, sol));

    if ((res = reduce(p, sol, smask)) < 0) {
        return res;
    } else if (res == 1) {
        goto BACKTRACK;
    }
    if (p->nclause == 0) {
        return 0;
    }
    u32 maxp = 0, maxv = 0;
    u8 sign = 0;

    for (u32f i = 0; i < p->nvars; i++) {
        if (p->pos[i] > maxv) {
            maxv = p->pos[i];
            maxp = i;
            sign = 1;
        }
        if (p->neg[i] > maxv) {
            maxv = p->pos[i];
            maxp = i;
            sign = 2;
        }
    }
    assert(sign);

    struct problem r = { .data = NULL, };

    if ((res = choose(p, maxp, sign, sol, &r)) < 0) {
        return res;
    } else if (res == 1) {
        goto BACKTRACK;
    }
    switch ((res = solve(&r, sol, smask))) {
        case 0: case -1:
            free(r.mem);
            return res;
    }
    if ((res = choose(p, maxp, (~sign)&3, sol, &r)) < 0) {
        return res;
    } else if (res == 1) {
        free(r.mem);
        goto BACKTRACK;
    }
    res = solve(&r, sol, smask);
    free(r.data);
    return res;
BACKTRACK:
    memset(smask, 0, pad_nvars);
    return 1;
}

static int __attribute__((unused))
nrsolve(struct problem *in, char *sol) {
    int res = -1;
    struct problem *q = calloc(in->nclause, sizeof(struct problem)), *p = q;
    u32f pad_nvars = (in->nvars * 2 + 7) / 8;
    u32 maxp = 0, maxv = 0;
    u8 sign = 0;
    u8 *smask = NULL;
    u32f nclause = in->nclause;


    if (!q || !(smask = calloc(1, pad_nvars))) {
        goto ERR_NRSOLVE;
    }
    *q = *in;
    DBG(showprob(p, sol));
FRESH:
    if ((res = reduce(p, sol, smask)) < 0) {
        goto ERR_NRSOLVE;
    } else if (res == 1) {
        goto BACKTRACK;
    }
    if (p->nclause == 0) {
        res = 0;
        goto ERR_NRSOLVE;
    }
    if (0) {
BACKTRACK:
        memset(smask, 0, pad_nvars);
        while (p != q) {
            (p--)->sign = 0;
            if (p->sign == 0) {
                continue;
            }
            goto OTHER;
        }
        res = 1;
        goto ERR_NRSOLVE;
    }
    maxp = 0, maxv = 0, sign = 0;
    for (u32f i = 0; i < p->nvars; i++) {
        if (p->pos[i] > maxv) {
            maxv = p->pos[i];
            maxp = i;
            sign = 1;
        }
        if (p->neg[i] > maxv) {
            maxv = p->pos[i];
            maxp = i;
            sign = 2;
        }
    }
    assert(sign);
    p->maxp = maxp;
    p->sign = sign;

    if ((res = choose(p, maxp, sign, sol, p + 1)) < 0) {
        goto ERR_NRSOLVE;
    } else if (res == 1) {
        goto BACKTRACK;
    }
    p++;
    assert(p->sign == 0);
    goto FRESH;
OTHER:
    maxp = p->maxp;
    sign = p->sign;
    p->sign = 0;
    if ((res = choose(p, maxp, (~sign)&3, sol, p + 1)) < 0) {
        return res;
    } else if (res == 1) {
        goto BACKTRACK;
    }
    p++;
    assert(p->sign == 0);
    goto FRESH;
ERR_NRSOLVE:
    if (q) {
        for (u32f i = 1; i < nclause; i++) {
            free(q[i].mem);
        }
    }
    free(q);
    free(smask);
    return res;
}

static int
reduce(struct problem *p, char *sol, u8 *smask) {
    int err = 1;
    u32f pad_nvars = (p->nvars * 2 + 7) / 8;
    assert((err = chkinvar(p)) == 0);
    do {
        if ((err = unitprop(p, sol)) == 1) {
            goto ERR_REDUCE;
        }
        u32f j = 0, simpl = 0;
        for (u32f i = 0; i < p->nvars; i++) {
            if (p->pos[i] + p->neg[i] == 0) {
                continue;
            }
            if (p->pos[i] == 0 || p->neg[i] == 0) {
                simpl++;
            }
        }
        if (!simpl) {
            err = 0;
            goto ERR_REDUCE;
        }
        for (u32f var = 0; var != p->nvars; var++) {
            u32 idx = 2 * var;
            if (p->pos[var] + p->neg[var] == 0) {
                continue;
            } else if (p->neg[var] == 0) {
                smask[idx / 8] |= 1 << (idx % 8);
                sol[var] = 1;
            } else if (p->pos[var] == 0) {
                smask[idx / 8] |= 2 << (idx % 8);
                sol[var] = -1;
            }
        }
        for (u32f i = 0; i < p->nclause; i++) {
            u8 *from = p->data + i * pad_nvars;
            u8 *into = p->data + j * pad_nvars;
            u8 ms = masksum(from, smask, pad_nvars);
            if (ms) {
                decrpos(from, p->pos, pad_nvars);
                decrneg(from, p->neg, pad_nvars);
            } else {
                memcpy(into, from, pad_nvars);
                p->clc[j] = p->clc[i];
                p->cln[j] = p->cln[i];
                j++;
            }
        }
        p->nclause = j;
        DBG(showprob(p, sol));
    } while (1);
ERR_REDUCE:
    return err;
}

static int
unitprop(struct problem *p, char *sol) {
    int err = 1;
    u32f pad_nvars = (p->nvars * 2 + 7) / 8;
    do {
        u8 sm = 0, im = 0;
        u32f simpl = 0, j = 0;
        u32f vbpos = 0, vpos = 0;
        for (u32f i = 0; i < p->nclause; i++) {
            if (p->clc[i] == 1) {
                u8 *c = p->data + i * pad_nvars;
                for (vbpos = 0; vbpos < pad_nvars; vbpos++) {
                    if (c[vbpos]) {
                        sm = c[vbpos];
                        im = ((sm << 1) & 0xaa) | ((sm >> 1) & 0x55);
                        vpos = vbpos * 4 + __builtin_ctz(sm) / 2;
                        break;
                    }
                }
                simpl = 1;
                break;
            }
        }
        if (!simpl) {
            break;
        }
        for (u32f i = 0; i < p->nclause; i++) {
            u8 *from = p->data + i * pad_nvars;
            u8 *into = p->data + j * pad_nvars;
            if (from[vbpos] & sm) {
                decrpos(from, p->pos, pad_nvars);
                decrneg(from, p->neg, pad_nvars);
            } else {
                memcpy(into, from, pad_nvars);
                if ((p->clc[j] = p->clc[i] - ((from[vbpos] & im) != 0)) == 0) {
                    goto ERR_UNIT;
                }
                into[vbpos] &= ~im;
                p->cln[j] = p->cln[i];
                j++;
            }
            p->pos[vpos] = 0;
            p->neg[vpos] = 0;
        }
        p->nclause = j;
        sol[vpos] = (sm & 0x55) ? 1 : -1;
        assert((err = chkinvar(p)) == 0);
        DBG(showprob(p, sol));
    } while (1);
    err = 0;
ERR_UNIT:
    return err;
}

static int
choose(struct problem *p, u32 vpos, u8 var, char *sol, struct problem *c) {
    int err = 1;
    u32f alloc;
    u32f pad_nvars = (p->nvars * 2 + 7) / 8;
    u32f pad_sign = ((p->nvars + 3) & -4);
    u32f nclause = p->nclause - (var&1 ? p->pos[vpos] : p->neg[vpos]);
    u32f nvars = p->nvars;
    u32f j = 0;
    u8 *mem;
    alloc = nclause * pad_nvars + sizeof(u32) * (2 * pad_sign + 2 * nclause);
    if (!(mem = realloc(c->mem, alloc))) {
        goto ERR_CHOOSE;
    }
    c->mem = mem;
    c->nvars = nvars;
    c->nclause = nclause;
    c->pos = (u32 *)c->mem;
    c->neg = c->pos + pad_sign;
    c->clc = c->neg + pad_sign;
    c->cln = c->clc + nclause;
    c->data = (u8 *)(c->cln + nclause);
    memcpy(c->pos, p->pos, (2 * pad_sign + nclause) * sizeof(u32));

    sol[vpos] = var == 1 ? 1 : -1;

    u32f vbpos = vpos / 4, vopos = vpos % 4;
    u8 smask = var << (2 * vopos);
    u8 pmask = ((~var)&3) << (2 * vopos);
    u8 imask = ~pmask;

    if (var == 1) {
        for (u32f i = 0; i < p->nclause; i++) {
            u8 *from = p->data + i * pad_nvars;
            u8 *into = c->data + j * pad_nvars;
            if (from[vbpos] & smask) {
                decrpos(from, c->pos, pad_nvars);
                decrneg(from, c->neg, pad_nvars);
            } else {
                memcpy(into, from, pad_nvars);
                u8 match = (from[vbpos] & pmask) >> (2 * vopos + 1);
                c->neg[vpos] -= match;
                assert(j < nclause);
                if ((c->clc[j] = p->clc[i] - match) == 0) {
                    goto ERR_CHOOSE;
                }
                into[vbpos] &= imask;
                c->cln[j] = p->cln[i];
                j++;
            }
        }
    } else {
        for (u32f i = 0; i < p->nclause; i++) {
            u8 *from = p->data + i * pad_nvars;
            u8 *into = c->data + j * pad_nvars;
            if (from[vbpos] & smask) {
                decrpos(from, c->pos, pad_nvars);
                decrneg(from, c->neg, pad_nvars);
            } else {
                memcpy(into, from, pad_nvars);
                u8 match = (from[vbpos] & pmask) >> (2 * vopos);
                c->pos[vpos] -= match;
                assert(j < nclause);
                if ((c->clc[j] = p->clc[i] - match) == 0) {
                    goto ERR_CHOOSE;
                }
                into[vbpos] &= imask;
                c->cln[j] = p->cln[i];
                j++;
            }
        }
    }

    DBG(assert((err = chkinvar(c)) == 0));
    DBG(showprob(c, sol));
    c->nclause = j;
    err = 0;
ERR_CHOOSE:
    return err;
}

static int
verify(struct problem *p, char *sol) {
    u32f pad_nvars = (p->nvars * 2 + 7) / 8;
    for (u32f c = 0; c < p->nclause; c++) {
        int sat = 0;
        for (u32f i = 0; i < p->nvars && !sat; i++) {
            u8 val = (p->data[i / 4 + c * pad_nvars] >> ((i % 4) * 2)) & 3;
            switch (val) {
                case 1: if (sol[i] == +1) sat = 1; break;
                case 2: if (sol[i] == -1) sat = 1; break;
            }
        }
        if (!sat) {
            warnx("unsat clause %ld", c + 1);
            return -1;
        }
    }
    return 0;
}

static int __attribute__((unused))
chkinvar(struct problem *p) {
    int err = 1;
    u32 *mm = calloc(2 * p->nvars + p->nclause, sizeof(u32));
    u32 *pos = mm;
    u32 *neg = pos + p->nvars;
    u32 *clc = neg + p->nvars;
    u32f pad_nvars = (p->nvars * 2 + 7) / 8;

    if (!mm) {
        return -1;
    }
    for (u32f c = 0; c < p->nclause; c++) {
        for (u32f i = 0; i < p->nvars; i++) {
            u8 val = (p->data[i / 4 + c * pad_nvars] >> ((i % 4) * 2)) & 3;
            switch (val) {
                case 0: break;
                case 1: pos[i]++; break;
                case 2: neg[i]++; break;
                case 3: err = -2; goto ERR_INVAR;
            }
        }
        clc[c] = popcnt(p->data + c * pad_nvars, pad_nvars);
    }
    for (u32f i = 0; i < p->nvars; i++) {
        if (p->pos[i] != pos[i]) {
            printf("pos[%lu]: exp %u, got %u\n", i, pos[i], p->pos[i]);
            err = -3;
            goto ERR_INVAR;
        }
    }
    for (u32f i = 0; i < p->nvars; i++) {
        if (p->neg[i] != neg[i]) {
            printf("neg[%lu]: exp %u, got %u\n", i, neg[i], p->neg[i]);
            return -4;
            goto ERR_INVAR;
        }
    }
    for (u32f i = 0; i < p->nclause; i++) {
        if (p->clc[i] != clc[i]) {
            printf("clc[%lu]: exp %u, got %u\n", i, clc[i], p->clc[i]);
            return -5;
            goto ERR_INVAR;
        }
    }
    err = 0;
ERR_INVAR:
    free(mm);
    return -err;
}

static void __attribute__((unused))
showprob(struct problem *p, char *sol) {
    u32 pad_nvars = (p->nvars * 2 + 7) / 8;
    printf("D:\n");
    for (u32f c = 0; c < p->nclause; c++) {
        printf("%3u: ", (u32)p->cln[c]);
        for (u32f i = 0; i < p->nvars; i++) {
            u8 val = (p->data[i / 4 + c * pad_nvars] >> ((i % 4) * 2)) & 3;
            switch (val) {
                case 0: printf("00 "); break;
                case 1: printf("01 "); break;
                case 2: printf("10 "); break;
                case 3: printf("11 "); break;
            }
        }
        printf("|% 4d\n", p->clc[c]);
    }
    printf("P: ");
    for (u32f i = 0; i < p->nvars; i++) {
       printf("%d ", p->pos[i]);
    }
    printf("\nN: ");
    for (u32f i = 0; i < p->nvars; i++) {
       printf("%d ", p->neg[i]);
    }
    printf("\nS: ");
    for (u32f i = 0; i < p->nvars; i++) {
       printf("%d ", sol[i]);
    }
    puts("");
}

static void
mkerr(struct errcnf *e, u32 l, u32 c, const char *se) {
    e->l = l;
    e->c = c;
    e->e = se;
}

static u8
masksum(u8 *restrict vec, u8 *restrict msk, u32f size) {
    u8 sum = 0;
    for (u32f i = 0; i < size; i++) {
        sum |= vec[i] & msk[i];
    }
    return sum;
}

static void
decrpos(u8 *vec, u32 *pos, u32f size) {
    for (u32f i = 0; i < size; i++, pos += 4) {
        u8 m = vec[i] & 0x55;
        if (m == 0) {
            continue;
        }
        const u32 *v = v4[m];
        for (u32f j = 0; j < 4; j++) {
            pos[j] -= v[j];
        }
    }
}

static void
decrneg(u8 *vec, u32 *neg, u32f size) {
    for (u32f i = 0; i < size; i++, neg += 4) {
        u8 m = vec[i] & 0xaa;
        if (m == 0) {
            continue;
        }
        const u32 *v = v4[m >> 1];
        for (u32f j = 0; j < 4; j++) {
            neg[j] -= v[j];
        }
    }
}

static u32
popcnt(u8 *vec, u32f size) {
    u32 res = 0;
    for (u32f i = 0; i < size; i++) {
        res += __builtin_popcount(vec[i]);
    }
    return res;
}

// LICENSE
// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.
//
// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to <http://unlicense.org/>
