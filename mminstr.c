// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE

// To compile in a single object file
// $gcc -DMMINSTR_IMPL -Wall -Wextra mminstr.c -c `pkg-config --cflags r_asm`
// To link to the object file
// $gcc main.c mminstr.o `pkg-config --libs r_asm` -lkeystone -o main
// When run a "mminstr.dump" file will be created at program exit with all the
// memory references addresses, with the most significant bit unset for reads,
// and set for writes.
// To print the dump in a readable format
// $xxd -e -g8 mminstr.dump

#define GUARD\
    asm volatile("":::"memory")
#define INSTR_ON\
    do {\
        mminstr();\
        GUARD;\
    } while (0)
#define INSTR_ON1\
    asm volatile(\
        "call mminstr1                          \n\t"\
        :::"memory"\
    )
#define INSTR_OFF\
    asm volatile(\
        "xchg %%ax, %%ax                        \n\t"\
        "xchg %%ax, %%ax                        \n\t"\
        "xchg %%ax, %%ax                        \n\t"\
        ".align 32                              \n\t"\
        "int $12                                \n\t"\
        :::"memory"\
    )
#define NOINLINE __attribute__((noinline))

__attribute__((noinline         )) void mminstr(void);
__attribute__((noinline, naked  )) void mminstr1(void);

#ifdef MMINSTR_IMPL
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/mman.h>
#include <r_asm.h>
#include <keystone/keystone.h>

#define ASSERT(x)\
    do {\
        if (__builtin_expect(!!(x), 1) == 0) {\
            fprintf(stderr, "Invariant violated %s:%d: %s\n", __FILE__,\
                __LINE__, #x);\
            exit(1);\
        }\
    } while (0)
#define INT_0xC     (0xccd)
#define PROT_ALL    (PROT_READ | PROT_WRITE | PROT_EXEC)
#define MAP_MEM     (MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED)
#define INSTR_MAX   (64)
#define NOPS_SIZE   (10)
#define ISM_MAX     (6)

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

enum jmpkind {
    JMP,
    JZ, JE = JZ,
    JNBE, JA = JNBE,
    JBE, JNA = JBE,
    JNZ, JNE = JNZ,
    JLE, JNG = JLE,
    JNL, JGE = JNL,
    JNLE, JG = JNLE,
    JL, JNGE = JL,
    JB, JNAE = JB, JC = JB,
    JNB, JAE = JNB, JNC = JNB,
    CALL,
    JO,
    JNO,
    JS,
    JNS,
    JP, JPE = JP,
    JNP, JPO = JNP,
    JECXZ, JRCXZ = JECXZ,
    INVALID_JMP, JMP_MAX = INVALID_JMP,
};

struct arr {
    size_t elesize;
    size_t size;
    size_t alloc;
    char data[];
};

struct jmpinfo {
    u8 *from;
    u8 *into;
    int len;
    enum jmpkind kind;
};

struct instrinfo {
    u8 instr[INSTR_MAX];
    int len;
    char repr[64];
    u8 *pc;
};

struct codemem {
    u8 *page_beg;
    u8 *code_beg;
    int code_len;
    u8 *icode;
    size_t isize;
};

struct instrmap {
    u8 *oldloc;
    u8 *newloc;
    int len;
};

struct instrmapseq {
    int len;
    struct instrmap seq[ISM_MAX];
};

static u8 *alloc_exe_page(u8 *, u8 *);
static int arr_push(struct arr **, const void *);
struct arr *arr_init(size_t, size_t);
static void *arr_get(struct arr *, size_t);
static char *eat_until(char *, char);
static int page_add(u8 *, int *, const void *, int);
static void instrinfo_show(const u8 *, int , const char *);
static void asm_instr(RAsm *, u8 *, int, u8 *, u8 *);
static u8 *alloc_exe_page(u8 *, u8 *);
static int icodemm(const char *, const u8 *, int, struct codemem *);
static int icode(char *, ks_engine *, struct codemem *);
static void dbg(const char *, ...);

static const u8 push_rax[1]             = "\x50";
static const u8 pop_rax[1]              = "\x58";
static const u8 push_rcx[1]             = "\x51";
static const u8 pop_rcx[1]              = "\x59";
static const u8 push_rdx[1]             = "\x52";
static const u8 pop_rdx[1]              = "\x5a";
static const u8 pushf[1]                = "\x9c";
static const u8 popf[1]                 = "\x9d";
static const u8 push_0[2]               = "\x6a\x00";
static const u8 push_1[2]               = "\x6a\x01";
static const u8 *const nops[NOPS_SIZE]  = {
    [0] = (u8 *)"",
    [1] = (u8 *)"\x90",
    [2] = (u8 *)"\x66\x90",
    [3] = (u8 *)"\x0f\x1f\x00",
    [4] = (u8 *)"\x0f\x1f\x40\x00",
    [5] = (u8 *)"\x0f\x1f\x44\x00\x00",
    [6] = (u8 *)"\x66\x0f\x1f\x44\x00\x00",
    [7] = (u8 *)"\x0f\x1f\x80\x00\x00\x00\x00",
    [8] = (u8 *)"\x0f\x1f\x84\x00\x00\x00\x00\x00",
    [9] = (u8 *)"\x66\x0f\x1f\x84\x00\x00\x00\x00\x00",
};
static u64 *refs  = NULL;
static size_t refssize  = 0;
static size_t refsalloc = 0;
static const char *const jmpstr[] = {
    [JO   ] = "jo",
    [JNO  ] = "jno",
    [JB   ] = "jb",
    [JAE  ] = "jae",
    [JE   ] = "je",
    [JNE  ] = "jne",
    [JBE  ] = "jbe",
    [JA   ] = "ja",
    [JS   ] = "js",
    [JNS  ] = "jns",
    [JP   ] = "jp",
    [JNP  ] = "jnp",
    [JL   ] = "jl",
    [JGE  ] = "jge",
    [JLE  ] = "jle",
    [JG   ] = "jg",
    [JECXZ] = "jecxz",
    [JMP  ] = "jmp",
    [CALL ] = "call"
};

#ifdef DBGDUMP
static void
dbg(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
}
#else
static void
dbg(const char *fmt, ...) {
    (void)fmt;
}
#endif

__attribute__((destructor)) void
dump(void) {
    int fd = open("mminstr.dump", O_RDWR | O_CREAT, 0644);
    ssize_t ret;

    if (fd == -1) {
        perror(__FILE__":dump@open");
        close(fd);
        return;
    }
    for (char *b = (char *)refs, *e = b + refssize; b != e; b += ret) {
        if ((ret = write(fd, b, e - b)) == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) continue;
            break;
        }
    }
    close(fd);
}

static int
arr_push(struct arr **parr, const void *it) {
    struct arr *arr = *parr;

    if (arr->size == arr->alloc) {
        size_t alloc = (arr->alloc + 1) * 2;
        arr = realloc(arr, sizeof(struct arr) + arr->elesize * alloc);
        if (!arr) return -1;
        *parr = arr;
        arr->alloc = alloc;
    }
    memcpy(arr_get(arr, arr->size++), it, arr->elesize);
    return 0;
}

struct arr *
arr_init(size_t elesize, size_t alloc) {
    struct arr *arr = malloc(sizeof(struct arr) + elesize * alloc);

    if (!arr) return NULL;
    arr->elesize = elesize;
    arr->alloc = alloc;
    arr->size = 0;
    return arr;
}

static void *
arr_get(struct arr *arr, size_t i) {
    return arr->data + i * arr->elesize;
}

static char *
eat_until(char *s, char c) {
    while (*s && *s != c) s++;
    return s;
}

static int
page_add(u8 *page_beg, int *occu, const void *code, int clen) {
    if (*occu + clen > 4096) return -1;
    memcpy(page_beg + *occu, code, clen);
    *occu += clen;
    return 0;
}

static void
instrinfo_show(const u8 *seccur, int ret, const char *strop) {
    dbg("%p | %-32s | ", seccur, strop);
    for (const u8 *b = seccur; b != seccur + ret; b++) {
        dbg("%02x", *b);
    }
    dbg("\n");
}

static size_t
strlcpy(char *dest, const char *src, size_t size) {
    size_t len = strlen(src);
    if (len == 0 || size == 0) return 0;
    if (len > size - 1) len = size -1;
    memcpy(dest, src, len);
    dest[len] = 0;
    return len;
}
    
static size_t
strnlcpy(char *dest, const char *src, size_t s, size_t size) {
    size_t len = strlen(src);
    if (len == 0 || size == 0) return 0;
    if (s > len) s = len;
    if (s > size - 1) s = size -1;
    memcpy(dest, src, s);
    dest[s] = 0;
    return s;
}

static int
icode(char *strcode, ks_engine *ks, struct codemem *cm) {
    size_t num;
    u64 pc = (u64)(cm->code_beg + cm->code_len);
    ks_err err = ks_asm(ks, strcode, pc, &(cm->icode), &(cm->isize), &num);
    ASSERT(err == 0 && num == 1);

    return icodemm(strcode, cm->icode, cm->isize, cm);
}


static int
icodemm(const char *str, const u8 *mm, int len, struct codemem *cm) {
    dbg("        %s\n", str);
    return page_add(cm->code_beg, &(cm->code_len), mm, len);
}

#define TRAMPOLINE(tok)\
        "   cmp $0, %%rdi                       \n\t"\
        "   jne REALFN"#tok"                    \n\t"\
        "   lea 1(%%rip), %%rax                 \n\t"\
        "   ret                                 \n\t"\
        "REALFN"#tok":                          \n\t"
#define SAVER(tok)\
        "   push %%rdx                          \n\t"\
        "   push %%rsi                          \n\t"\
        "   push %%rdi                          \n\t"\
        "   mov %[refs], %%rdi                  \n\t"\
        "   mov %[refssize], %%rdx              \n\t"\
        "   mov %[refsalloc], %%rsi             \n\t"\
        "   cmp %%rdx, %%rsi                    \n\t"\
        "   je REALLOC"#tok"                    \n\t"\
        "ADDREF"#tok":                          \n\t"\
        "   mov 24(%%rsp), %%rsi                \n\t"\
        "   shl $63, %%rsi                      \n\t"\
        "   or %%rsi, %%rax                     \n\t"\
        "   mov %%rax, (%%rdi, %%rdx)           \n\t"\
        "   add $8, %%rdx                       \n\t"\
        "   mov %%rdx, %[refssize]              \n\t"\
        "   pop %%rdi                           \n\t"\
        "   pop %%rsi                           \n\t"\
        "   pop %%rdx                           \n\t"\
        "   ret                                 \n\t"\
        "REALLOC"#tok":                         \n\t"\
        "   lea 8(%%rdx, %%rsi, 2), %%rsi       \n\t"\
        "   mov %%rsi, %[refsalloc]             \n\t"\
        "# save all non-preserved regs here     \n\t"\
        "   push %%rax                          \n\t"\
        "   push %%rdx                          \n\t"\
        "   push %%rcx                          \n\t"\
        "   push %%r8                           \n\t"\
        "   push %%r9                           \n\t"\
        "   push %%r10                          \n\t"\
        "   push %%r11                          \n\t"\
        "# align 16 rsp                         \n\t"\
        "   push %%rsp                          \n\t"\
        "   push (%%rsp)                        \n\t"\
        "   and $-16, %%rsp                     \n\t"\
        "   call realloc                        \n\t"\
        "   mov %%rax, %[refs]                  \n\t"\
        "   mov %%rax, %%rdi                    \n\t"\
        "# restore rsp                          \n\t"\
        "   mov 8(%%rsp), %%rsp                 \n\t"\
        "# restore all non-preserved regs here  \n\t"\
        "   pop %%r11                           \n\t"\
        "   pop %%r10                           \n\t"\
        "   pop %%r9                            \n\t"\
        "   pop %%r8                            \n\t"\
        "   pop %%rcx                           \n\t"\
        "   pop %%rdx                           \n\t"\
        "   pop %%rax                           \n\t"\
        "   jmp ADDREF"#tok"                    \n\t"

__attribute__((naked, unused)) static u64
saver(__attribute__((unused)) u8 *addr) {
    asm volatile(
        TRAMPOLINE(saver)
        SAVER(saver)
        : [refs]        "=m" (refs)
        , [refssize]    "=m" (refssize)
        , [refsalloc]   "=m" (refsalloc)
        ::
    );
#ifndef __clang__
    return 0;
#endif
}

__attribute__((naked, unused)) static u64
thunk_saver(__attribute__((unused)) u8 *addr) {
    asm volatile(
        SAVER(thunk_saver)
        : [refs]        "=m" (refs)
        , [refssize]    "=m" (refssize)
        , [refsalloc]   "=m" (refsalloc)
        ::
    );
#ifndef __clang__
    return 0;
#endif
}

__attribute__((naked, unused)) static u64
saver_trampoline(__attribute__((unused)) u8 *addr) {
    asm volatile(
        TRAMPOLINE(saver_trampoline)
        "call thunk_saver                       \n\t"
        "ret                                    \n\t"
        :::
    );
#ifndef __clang__
    return 0;
#endif
}
#undef TRAMPOLINE
#undef SAVER

static enum jmpkind
jmpclass(const char *str) {
    enum jmpkind ret = INVALID_JMP;

    for (size_t i = 0; i < JMP_MAX; i++) {
        size_t len = strlen(jmpstr[i]);
        if (strncmp(str, jmpstr[i], len) == 0 && str[len] == ' ') {
            ret = i;
            break;
        }
    }
    return ret;
}

void
call_del(RAsm *a, u8 *call) {
    RAsmOp op;
    u8 *secend = call + 1;

    while (1) {
        ASSERT(call);
        r_asm_set_pc(a, (u64)call);
        int ret = r_asm_disassemble(a, &op, call, secend - call);
        char *strop = r_strbuf_get(&op.buf_asm);
        if (strncmp(strop, "call", sizeof("call") - 1) == 0) {
            dbg("[nop]: %s\n", strop);
            if (ret < NOPS_SIZE) memcpy(call, nops[ret], ret);
            else memset(call, *nops[1], ret);
            break;
        }
        call--;
    }
}

static void
asm_instr(RAsm *a, u8 *buf, int len, u8 *page_beg, u8 *page_end) {
    RAsmOp op;
    u64 pc = (u64)buf;
    static char arr[64];
    static struct instrinfo instrs[6] = { };
    struct codemem cm = { .page_beg = page_beg, };
    size_t stat;
    ks_engine *ks;

    ks_err err = ks_open(KS_ARCH_X86, KS_MODE_64, &ks);
    ASSERT(err == 0);
    ks_option(ks, KS_OPT_SYNTAX, KS_OPT_SYNTAX_NASM);

    r_asm_set_pc(a, pc);

    u8 *secbeg = buf;
    u8 *secend = secbeg + len;

    refsalloc = (secend - secbeg) * 5 * sizeof(u64);
    refs = realloc(refs, refsalloc);

    void (*fn)(u64) = (void *)saver(NULL);

    struct arr *patch_jmp = arr_init(sizeof(struct jmpinfo),
        (secend - secbeg) / 24
    );
    struct arr *nopatch_jmp = arr_init(sizeof(struct jmpinfo),
        (secend - secbeg) / 24
    );
    struct arr *instrmaparr = arr_init(sizeof(struct instrmapseq),
        (secend - secbeg) / 24
    );

    for (u8 *seccur = secbeg; seccur < secend;) {
        r_asm_set_pc(a, (u64)seccur);
        int ret = r_asm_disassemble(a, &op, seccur, secend - seccur);
        char *strop = r_strbuf_get(&op.buf_asm);

        if(ret < 1) ret = 1;

        if (*strop == 'j' || strncmp(strop, "call", sizeof("call") - 1) == 0) {
            enum jmpkind jk = jmpclass(strop);
            ASSERT(jk != INVALID_JMP);
            char *beg = eat_until(strop, ' '), *ebeg = beg;
            ASSERT(*beg == ' ');
            char *end = eat_until(strop, '\0');
            u64 dest = strtoll(beg, &ebeg, 16);
            struct jmpinfo ji = (struct jmpinfo) {
                .from = seccur,
                .into = (void *)dest,
                .len = ret,
                .kind = jk,
            };
            if (ebeg != end) {
                dbg("cannot instrument %p, unpredicatable %s@%p", buf, strop,
                    seccur
                );
                goto ASM_INSTR_EXIT;
            }
            arr_push(ret >= 6 ? &patch_jmp : &nopatch_jmp, &ji);
        }
        seccur += ret;
    }
    dbg("[patchable jmps]:\n");
    for (size_t i = 0; i < patch_jmp->size; i++) {
        struct jmpinfo *it = arr_get(patch_jmp, i);
        dbg("%p | %s 0x%p\n", it->from, jmpstr[it->kind], it->into);
    }
    dbg("[non-pacthable jmps]:\n");
    for (size_t i = 0; i < nopatch_jmp->size; i++) {
        struct jmpinfo *it = arr_get(nopatch_jmp, i);
        dbg("%p | %s 0x%p\n", it->from, jmpstr[it->kind], it->into);
    }
    dbg("[addr sequences]:\n");
RESTART_INSTR:
    cm.code_beg = alloc_exe_page(cm.page_beg, page_end);
    cm.code_len = 0;

    for (u8 *seccur = secbeg; seccur < secend;) {
        u8 *iseqbeg = seccur;
        u8 *piseqbeg = cm.code_beg + cm.code_len;
        int seqlen = 0;
        int skip = 0;

        r_asm_set_pc(a, (u64)seccur);
        int ret = r_asm_disassemble(a, &op, seccur, secend - seccur);
        if(ret < 1) ret = 1;
        seccur += ret;

        char *strop = r_strbuf_get(&op.buf_asm);
        instrinfo_show(seccur - ret, ret, strop);

        if (strncmp(strop, "nop", sizeof("nop") - 1) == 0) {
            continue;
        }
        if (*eat_until(strop, '[') != '[') {
            continue;
        }
        if (strstr(strop, "rip") != NULL) {
            if (*strop == 'j' || // ignore rip relative jumps
                strncmp(strop, "call", sizeof("call") - 1) == 0 || // calls
                strstr(strop, "rsp") != NULL // ops also affecting rsp
            ) {
                continue;
            }
        }
        struct instrinfo *instr = instrs + seqlen++;
        dbg("%s\n", strop);

        if (ret > INSTR_MAX) {
            continue;
        } else {
            instr->len = ret;
            instr->pc = seccur - ret;
            strlcpy(instr->repr, strop, sizeof(instr->repr));
            memcpy(instr->instr, seccur - ret, ret);
        }
        while (seccur - iseqbeg < 5) {
            instr = instrs + seqlen++;

            r_asm_set_pc(a, (u64)seccur);
            int ret = r_asm_disassemble(a, &op, seccur, secend - seccur);
            if (ret < 1) ret = 1;
            seccur += ret;

            char *strop = r_strbuf_get(&op.buf_asm);
            dbg("%s\n", strop);

            if (strstr(strop, "rip") != NULL) {
                if (*strop == 'j' || // ignore rip relative jumps
                    strncmp(strop, "call", sizeof("call") - 1) == 0 || // calls
                    strstr(strop, "rsp") != NULL // ops also affecting rsp
                ) {
                    skip = 1;
                    break;
                }
            }
            for (size_t i = 0; i < nopatch_jmp->size; i++) {
                struct jmpinfo *it = arr_get(nopatch_jmp, i);
                if (it->into == seccur - ret || it->from == seccur - ret) {
                    skip = 1;
                    break;
                }
            }
            if (ret > INSTR_MAX) {
                skip = 1;
                break;
            } else {
                instr->len = ret;
                instr->pc = seccur - ret;
                strlcpy(instr->repr, strop, sizeof(instr->repr));
                memcpy(instr->instr, seccur - ret, ret);
            }
        }
        if (skip) continue;

        u8 *iseqend = seccur;
        struct instrmapseq ims = { .len = seqlen };

        for (int instridx = 0; instridx < seqlen; instridx++) {
            struct instrinfo *iinfo = instrs + instridx;
            dbg("    %s\n", iinfo->repr);

            char *beg = eat_until(iinfo->repr, '[');
            char *end = eat_until(beg, ']');
            char *rip = strstr(iinfo->repr, "rip");
            char *rcx = strstr(iinfo->repr, "rcx");

            int ismem = *beg == '[';
            int iswrt = *eat_until(end, ',') == ',';
            int isrip = rip != NULL;
            int isrcx = rcx != NULL;

            if (isrip) memcpy(rip, isrcx ? "rdx" : "rcx", 3);
            u8 *code = cm.code_beg + cm.code_len;

            if (ismem) {
                if (isrip) {
                    if (isrcx) {
                        if (icodemm("push rdx", push_rdx, 1, &cm)) {
                            goto RESTART_INSTR;
                        }
                    } else {
                        if (icodemm("push rcx", push_rcx, 1, &cm)) {
                            goto RESTART_INSTR;
                        }
                    }
                }
                if (icodemm("pushf", pushf, 1, &cm)) goto RESTART_INSTR;
                if (isrip) {
                    if (isrcx) {
                        sprintf(arr, "mov rdx, %p", iinfo->pc + iinfo->len);
                    } else {
                        sprintf(arr, "mov rcx, %p", iinfo->pc + iinfo->len);
                    }
                    if (icode(arr, ks, &cm)) goto RESTART_INSTR;
                }
                if (icodemm("push rax", push_rax, 1, &cm)) goto RESTART_INSTR;
                if (iswrt) {
                    if (icodemm("push 1", push_1, 2, &cm)) goto RESTART_INSTR;
                } else {
                    if (icodemm("push 0", push_0, 2, &cm)) goto RESTART_INSTR;
                }
                size_t s = strlcpy(arr, "lea rax, ", sizeof(arr));
                strnlcpy(arr + s, beg, end - beg + 1,
                    sizeof(arr) - (end - beg + 1)
                );
                if (icode(arr, ks, &cm)) goto RESTART_INSTR;

                sprintf(arr, "call %p", fn);
                if (icode(arr, ks, &cm)) goto RESTART_INSTR;
                if (icodemm("pop rax", pop_rax, 1, &cm)) goto RESTART_INSTR;
                if (icodemm("pop rax", pop_rax, 1, &cm)) goto RESTART_INSTR;
                if (icodemm("popf", popf, 1, &cm)) goto RESTART_INSTR;
            }
            ims.seq[instridx] = (struct instrmap) {
                .oldloc = iinfo->pc,
                .newloc = ismem ? code : cm.code_beg + cm.code_len,
                .len    = iinfo->len,
            };
            if (isrip) {
                if (icode(iinfo->repr, ks, &cm)) goto RESTART_INSTR;
                if (isrcx) {
                    if (icodemm("pop rdx", pop_rdx, 1, &cm)) goto RESTART_INSTR;
                } else {
                    if (icodemm("pop rcx", pop_rcx, 1, &cm)) goto RESTART_INSTR;
                }
            } else {
                if (icodemm(iinfo->repr, iinfo->instr, iinfo->len, &cm)) {
                    goto RESTART_INSTR;
                }
            }
        }
        u8 *jmp_to = iseqend;
        sprintf(arr, "jmp %p", jmp_to);
        if (icode(arr, ks, &cm)) goto RESTART_INSTR;

        arr_push(&instrmaparr, &ims);

        u64 pc = (u64)iseqbeg;
        sprintf(arr, "jmp %p", piseqbeg);
        err = ks_asm(ks, arr, pc, &cm.icode, &cm.isize, &stat);
        ASSERT(err == 0 && stat == 1);
        memcpy(iseqbeg, cm.icode, cm.isize);

        iseqbeg += cm.isize;
        while (iseqbeg + NOPS_SIZE - 1 < iseqbeg) {
            memcpy(iseqbeg, nops[NOPS_SIZE - 1], NOPS_SIZE - 1);
            iseqbeg += NOPS_SIZE -1;
        }
        memcpy(iseqbeg, nops[iseqend - iseqbeg], iseqend - iseqbeg);
    }

    for (size_t i = 0; i < patch_jmp->size; i++) {
        struct jmpinfo *jmp = arr_get(patch_jmp, i);
        struct instrmap *from = NULL, *into = NULL;

        for (size_t j = 0; j < instrmaparr->size; j++) {
            struct instrmapseq *ims = arr_get(instrmaparr, j);
            struct instrmap *last = ims->seq + ims->len - 1;
            u8 *ims_beg = ims->seq[0].oldloc;
            u8 *ims_end = last->oldloc + last->len;
            if ((jmp->from < ims_beg || jmp->from >= ims_end) &&
                (jmp->into < ims_beg || jmp->into >= ims_end)
            ) {
                continue;
            }
            for (int k = 0; k < ims->len; k++) {
                struct instrmap *im = ims->seq + k;
                if (jmp->from == im->oldloc) {
                    from = im;
                } else if (jmp->into == im->oldloc) {
                    into = im;
                }
            }
            ASSERT(from || into);
        }
        if (from == NULL && into == NULL) continue;

        u8 *jmpfrom = from ? from->newloc : jmp->from;
        u8 *jmpinto = into ? into->newloc : jmp->into;
        int jmplen  = jmp->len;

        memset(jmpfrom, *nops[1], jmplen);
        sprintf(arr, "%s %p", jmpstr[jmp->kind], jmpinto);
        ks_asm(ks, arr, (u64)jmpfrom, &cm.icode, &cm.isize, &stat);

        ASSERT((size_t)jmplen >= cm.isize);
        memcpy(jmpfrom, cm.icode, cm.isize);
    }
ASM_INSTR_EXIT:
    ks_free(cm.icode);
    ks_close(ks);
}

static u8 *
alloc_exe_page(u8 *near_to, u8 *search_from) {
#ifndef LINUX_ONLY
    (void)search_from;

    static const size_t alen = 4096;
    int fd = open("/proc/self/maps", O_RDONLY);
    char *mm = NULL;
    size_t flen = 0;
    ssize_t llen;
    do {
        mm = realloc(mm, flen + 0x1000 + 1);
        ASSERT(mm);
        if ((llen = read(fd, mm + flen, 0x1000)) == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) continue;
            else break;
        }
        flen += llen;
    } while (llen);
    close(fd);
    mm[flen] = 0;

    char *curr = mm;
    u8 *seg = NULL, *ret = NULL;

    while (*curr) {
        char *ebeg;
        u8 *beg = (u8 *)strtoll(curr, &ebeg, 16);
        ASSERT(ebeg > curr);
        curr = ebeg + 1;
        u8 *end = (u8 *)strtoll(curr, &ebeg, 16);
        ASSERT(ebeg > curr);

        if (seg && seg != beg && seg - near_to + alen <= 0xffffffff &&
            mmap(seg, alen, PROT_ALL, MAP_MEM, -1, 0) != MAP_FAILED
        ) {
            ret = seg;
            break;
        }
        seg = end;

        char *lend = curr;
        while (*lend && *lend != '\n') lend++;
        curr = lend;
        if (*curr) curr++;
    }
    free(mm);

    if (!ret) {
        while (seg - near_to + alen <= 0xffffffff) {
            if (mmap(seg, alen, PROT_ALL, MAP_MEM, -1, 0) != MAP_FAILED) {
                ret = seg;
                break;
            }
            seg += alen;
        }
    }
    return ret;
#else
    u8 *page = search_from;
    static const size_t len = 4096;
    u8 vec[0x2000];

    while (1) {
        if (page - near_to + len > 0xffffffff) {
            return NULL;
        }
        if (mincore(page, len, vec) == -1 && errno == ENOMEM) {
            if (mmap(page, len, PROT_ALL, MAP_MEM, -1, 0) != MAP_FAILED) {
                break;
            }
        }
        page += len;
    }
    return page;
#endif
}

static int lock;

#define LOCK(lock) while(!__sync_bool_compare_and_swap(&lock, 0, 1));
#define UNLOCK(lock)\
    do {\
        asm volatile ("":::"memory");\
        *(volatile int *)(&lock) = 0;\
    } while (0)
#define RSAVE_ALL(text_beg)\
    asm volatile(\
        "pushf                                  \n\t"\
        "lea -992(%%rsp), %%rsp                 \n\t"\
        "mov %%rdi,  0(%%rsp)                   \n\t"\
        "mov %%rsi,  8(%%rsp)                   \n\t"\
        "mov %%rdx,  6(%%rsp)                   \n\t"\
        "mov %%rcx, 24(%%rsp)                   \n\t"\
        "mov %%r8 , 32(%%rsp)                   \n\t"\
        "mov %%r9 , 40(%%rsp)                   \n\t"\
        "mov %%r10, 48(%%rsp)                   \n\t"\
        "vmovdqu %%xmm0,  56(%%rsp)             \n\t"\
        "vmovdqu %%xmm1,  72(%%rsp)             \n\t"\
        "vmovdqu %%xmm2,  88(%%rsp)             \n\t"\
        "vmovdqu %%xmm3, 104(%%rsp)             \n\t"\
        "vmovdqu %%xmm4, 120(%%rsp)             \n\t"\
        "vmovdqu %%xmm5, 136(%%rsp)             \n\t"\
        "vmovdqu %%xmm6, 152(%%rsp)             \n\t"\
        "vmovdqu %%xmm7, 168(%%rsp)             \n\t"\
        "mov %%rax,  184(%%rsp)                 \n\t"\
        "mov %%rbx,  192(%%rsp)                 \n\t"\
        "mov %%r11,  216(%%rsp)                 \n\t"\
        "mov %%r12,  224(%%rsp)                 \n\t"\
        "mov %%r13,  232(%%rsp)                 \n\t"\
        "mov %%r14,  240(%%rsp)                 \n\t"\
        "mov %%r15,  248(%%rsp)                 \n\t"\
        "mov %%rbp,  256(%%rsp)                 \n\t"\
        "lea 272(%%rsp), %%rbp                  \n\t"\
        "mov 1000(%%rsp), %0                    \n\t"\
        :"=r"(text_beg)\
    )
#define RLOAD_ALL\
    asm volatile(\
        "mov  0(%rsp), %rdi                     \n\t"\
        "mov  8(%rsp), %rsi                     \n\t"\
        "mov 16(%rsp), %rdx                     \n\t"\
        "mov 24(%rsp), %rcx                     \n\t"\
        "mov 32(%rsp), %r8                      \n\t"\
        "mov 40(%rsp), %r9                      \n\t"\
        "mov 48(%rsp), %r10                     \n\t"\
        "vmovdqu  56(%rsp), %xmm0               \n\t"\
        "vmovdqu  72(%rsp), %xmm1               \n\t"\
        "vmovdqu  88(%rsp), %xmm2               \n\t"\
        "vmovdqu 104(%rsp), %xmm3               \n\t"\
        "vmovdqu 120(%rsp), %xmm4               \n\t"\
        "vmovdqu 136(%rsp), %xmm5               \n\t"\
        "vmovdqu 152(%rsp), %xmm6               \n\t"\
        "vmovdqu 168(%rsp), %xmm7               \n\t"\
        "mov 184(%rsp), %rax                    \n\t"\
        "mov 192(%rsp), %rbx                    \n\t"\
        "mov 216(%rsp), %r11                    \n\t"\
        "mov 224(%rsp), %r12                    \n\t"\
        "mov 232(%rsp), %r13                    \n\t"\
        "mov 240(%rsp), %r14                    \n\t"\
        "mov 248(%rsp), %r15                    \n\t"\
        "mov 256(%rsp), %rbp                    \n\t"\
        "lea 992(%rsp), %rsp                    \n\t"\
        "popf                                   \n\t"\
        "ret                                    \n\t"\
    )

__attribute__((noinline)) void
mminstr(void) {
    LOCK(lock);
    u8 *text_beg = __builtin_return_address(0);
    static u8 *addr_instr_beg[4096], **addr_instr_end = addr_instr_beg;

    for (u8 **addr = addr_instr_beg; addr < addr_instr_end; addr++) {
        if (*addr == text_beg) {
            goto MMINSTREND;
        }
    }
    u16 *end = (u16 *)(((u64)text_beg + 31ll) & ~31ll);
    while (*end != INT_0xC) {
        end += 16;
    }
    u8 *text_end = (u8 *)end;
    u8 *page_beg = (u8 *)(((u64)text_beg - 4095ll) & ~4095ll);
    u8 *page_end = page_beg;

    do {
        page_end += 4096;
    } while (page_end <= text_end);

    mprotect(page_beg, page_end - page_beg, PROT_ALL);
    memcpy(end, nops[2], 2);

    RAsm *a = r_asm_new();
    r_asm_set_arch(a, "x86", 64);
    call_del(a, text_beg - 1);
    asm_instr(a, text_beg, text_end - text_beg, page_beg, page_end);
    r_asm_free(a);

    *addr_instr_end++ = text_beg;
MMINSTREND:
    UNLOCK(lock);
}

#ifndef __clang__
__attribute__((noinline, naked)) void
mminstr1(void) {
    u8 *text_beg;
    RSAVE_ALL(text_beg);
    LOCK(lock);

    static u8 *addr_instr_beg[4096], **addr_instr_end = addr_instr_beg;

    for (u8 **addr = addr_instr_beg; addr < addr_instr_end; addr++) {
        if (*addr == text_beg) {
            goto MMINSTREND;
        }
    }
    u16 *end = (u16 *)(((u64)text_beg + 31ll) & ~31ll);
    while (*end != INT_0xC) {
        end += 16;
    }
    u8 *text_end = (u8 *)end;
    u8 *page_beg = (u8 *)(((u64)text_beg - 4095ll) & ~4095ll);
    u8 *page_end = page_beg;

    do {
        page_end += 4096;
    } while (page_end <= text_end);

    mprotect(page_beg, page_end - page_beg, PROT_ALL);
    memcpy(end, nops[2], 2);

    RAsm *a = r_asm_new();
    r_asm_set_arch(a, "x86", 64);
    call_del(a, text_beg - 1);
    asm_instr(a, text_beg, text_end - text_beg, page_beg, page_end);
    r_asm_free(a);

    *addr_instr_end++ = text_beg;
MMINSTREND:
    UNLOCK(lock);
    RLOAD_ALL;
}
#endif
#undef LOCK
#undef UNLOCK
#undef RSAVE_ALL
#undef RLOAD_ALL

#undef ASSERT
#undef INT_0xC
#undef PROT_ALL
#undef MAP_MEM
#undef INSTR_MAX
#undef ISM_MAX
#endif // MMINSTR_IMPL

// LICENSE
// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.
//
// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to <http://unlicense.org/>


