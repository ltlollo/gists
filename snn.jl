using FileIO, Images, ImageView, JLD

const M32 = Matrix{Float32}

mutable struct NNParams
	alpha		:: Float32
	beta1		:: Float32
	beta2		:: Float32
	niter		:: Int64
	costerr		:: Float32
	layer_size	:: Vector{ Int32 }
	keep_prob	:: Float32
end

mutable struct NNMom
	VdW	:: Vector{ M32 }
	Vdb	:: Vector{ M32 }
	SdW	:: Vector{ M32 }
	Sdb	:: Vector{ M32 }
	NNMom(sizes) = begin
		nlayers = size(sizes, 1) - 1
		VdW = Vector{ M32 }(nlayers)
		Vdb = Vector{ M32 }(nlayers)
		SdW = Vector{ M32 }(nlayers)
		Sdb = Vector{ M32 }(nlayers)
		for i = 1:nlayers
			VdW[i] = zeros(Float32, sizes[i+1], sizes[i])
			Vdb[i] = zeros(Float32, sizes[i+1], 1)
			SdW[i] = zeros(Float32, sizes[i+1], sizes[i])
			Sdb[i] = zeros(Float32, sizes[i+1], 1)
		end
		return new(VdW, Vdb, SdW, Sdb)
	end
end

mutable struct NNState
	W	:: Vector{ M32 }
	b	:: Vector{ M32 }
	NNState(sizes) = begin
		nlayers = size(sizes, 1) - 1
		W, b = fieldtype(NNState, :W)(nlayers), fieldtype(NNState, :b)(nlayers) 
		for i = 1:nlayers
			W[i] = randn(Float32, sizes[i+1], sizes[i]) * sqrt(2.f0/sizes[i])
			b[i] = zeros(Float32, sizes[i+1], 1)
		end
		return new(W, b)
	end
end

mutable struct NNInterm
	A	:: Vector{ M32 }
	Z	:: Vector{ M32 }
	D	:: Vector{ M32 }
	NNInterm(sizes) = begin
		nlayers = size(sizes, 1) - 1
		A = Vector{ M32 }(nlayers+1)
		Z = Vector{ M32 }(nlayers)
		D = Vector{ M32 }(nlayers-1)
		return new(A, Z, D)
	end
end

struct NNStat
	mu	:: M32
	s2	:: M32
end

red, green, blue = a->Float32(a.r), a->Float32(a.g), a->Float32(a.b)

imgvec(img) = [vec(red.(img)); vec(green.(img)); vec(blue.(img))]

sigmoid(z :: Float32) = 1.f0/(1.f0+e^-z)

flatten(X :: M32) = mapslices(norm, X, 1)/size(X, 1)

imgresize(fn) = mktemp((pa, io)-> begin run(pipeline(`convert "$fn"
-resize 64x64\> -resize 64x64^ -gravity center -extent 64x64
-format png -`, io)); return load(File(format"PNG", pa)) end)

initnn(params :: NNParams) = (
	NNState(params.layer_size),
	NNInterm(params.layer_size),
	NNMom(params.layer_size),
)

fcost(A :: M32, Y :: M32) = -sum(Y .* log.(A)) / size(Y, 2)

function snormalize!(v, stat)
	v .-= stat.mu
	v ./= stat.s2
end

function softmax(v :: M32)
	res = e.^v
	return res ./ sum(res, 1)
end

function fwprop!(
	state	:: NNState,
	input	:: M32,
	interm	:: NNInterm,

	keep	:: Float32 = 1.f0,
	)
	interm.A[1] = input
	for i = 1:size(state.W, 1)-1
		zi = state.W[i] * interm.A[i] .+ state.b[i]
		di = (rand(Float32, size(zi)) .< keep) / keep
		ai = max.(0.f0, zi) .* di
		interm.Z[i] = zi
		interm.A[i+1] = ai
		interm.D[i] = di
	end
	i = size(state.W, 1)
	zi = state.W[i] * interm.A[i] .+ state.b[i]
	ai = sigmoid.(zi)
	interm.Z[i] = zi
	interm.A[i+1] = ai
end

function ewabkprop!(
	state	:: NNState,
	output	:: M32,
	interm	:: NNInterm,
	mom		:: NNMom,
	alpha	:: Float32,
	beta1	:: Float32,
	)
	i = size(state.W, 1)
	m :: Float32 = size(output, 2)
	dZi = interm.A[i+1] - output
	dWi = 1.f0/m * A_mul_Bt(dZi,  interm.A[i])
	dbi = 1.f0/m * sum(dZi, 2)
	mom.VdW[i] = beta1 * mom.VdW[i] + (1.f0-beta1) * dWi
	mom.Vdb[i] = beta1 * mom.Vdb[i] + (1.f0-beta1) * dbi
	dAp = At_mul_B(state.W[i],  dZi) .* interm.D[i-1]
	state.W[i] -= alpha * mom.VdW[i]
	state.b[i] -= alpha * mom.Vdb[i]
	l = i-1
	for i = l:-1:2
		dZi = dAp .* (interm.Z[i] .> 0f0)
		dWi = 1.f0/m * A_mul_Bt(dZi, interm.A[i])
		dbi = 1.f0/m * sum(dZi, 2)
		mom.VdW[i] = beta1 * mom.VdW[i] + (1.f0-beta1) * dWi
		mom.Vdb[i] = beta1 * mom.Vdb[i] + (1.f0-beta1) * dbi
		dAp = At_mul_B(state.W[i], dZi) .* interm.D[i-1]
		state.W[i] -= alpha * mom.VdW[i]
		state.b[i] -= alpha * mom.Vdb[i]
	end
	i = 1
	dZi = dAp .* (interm.Z[i] .> 0f0)
	dWi = 1.f0/m * A_mul_Bt(dZi, interm.A[i])
	dbi = 1.f0/m * sum(dZi, 2)
	mom.VdW[i] = beta1 * mom.VdW[i] + (1.f0-beta1) * dWi
	mom.Vdb[i] = beta1 * mom.Vdb[i] + (1.f0-beta1) * dbi
	state.W[i] -= alpha * mom.VdW[i]
	state.b[i] -= alpha * mom.Vdb[i]
end

function adambkprop!(
	state	:: NNState,
	output	:: M32,
	interm	:: NNInterm,
	mom		:: NNMom,
	alpha	:: Float32,
	beta1	:: Float32,
	beta2	:: Float32,
	nstep	:: Int64,
	)
	i = size(state.W, 1)
	t = nstep
	m :: Float32 = size(output, 2)
	dZi = interm.A[i+1] - output
	dWi = 1.f0/m * A_mul_Bt(dZi,  interm.A[i])
	dbi = 1.f0/m * sum(dZi, 2)
	mom.VdW[i] = beta1 * mom.VdW[i] + (1.f0-beta1) * dWi
	mom.Vdb[i] = beta1 * mom.Vdb[i] + (1.f0-beta1) * dbi
	mom.SdW[i] = beta2 * mom.SdW[i] + (1.f0-beta2) * dWi.^2
	mom.Sdb[i] = beta2 * mom.Sdb[i] + (1.f0-beta2) * dbi.^2
	dAp = At_mul_B(state.W[i],  dZi) .* interm.D[i-1]
	state.W[i] -= (alpha * mom.VdW[i] / (1-beta1^t)) ./
		(sqrt.(mom.SdW[i] / (1-beta2^t) + 10f-8))
	state.b[i] -= (alpha * mom.Vdb[i] / (1-beta1^t)) ./
		(sqrt.(mom.Sdb[i] / (1-beta2^t) + 10f-8))
	l = i-1
	for i = l:-1:2
		dZi = dAp .* (interm.Z[i] .> 0f0)
		dWi = 1.f0/m * A_mul_Bt(dZi, interm.A[i])
		dbi = 1.f0/m * sum(dZi, 2)
		mom.VdW[i] = beta1 * mom.VdW[i] + (1.f0-beta1) * dWi
		mom.Vdb[i] = beta1 * mom.Vdb[i] + (1.f0-beta1) * dbi
		mom.SdW[i] = beta2 * mom.SdW[i] + (1.f0-beta2) * dWi.^2
		mom.Sdb[i] = beta2 * mom.Sdb[i] + (1.f0-beta2) * dbi.^2
		dAp = At_mul_B(state.W[i], dZi) .* interm.D[i-1]
		state.W[i] -= (alpha * mom.VdW[i] / (1-beta1^t)) ./
			(sqrt.(mom.SdW[i] / (1-beta2^t) + 10f-8))
		state.b[i] -= (alpha * mom.Vdb[i] / (1-beta1^t)) ./
			(sqrt.(mom.Sdb[i] / (1-beta2^t) + 10f-8))
	end
	i = 1
	dZi = dAp .* (interm.Z[i] .> 0f0)
	dWi = 1.f0/m * A_mul_Bt(dZi, interm.A[i])
	dbi = 1.f0/m * sum(dZi, 2)
	mom.VdW[i] = beta1 * mom.VdW[i] + (1.f0-beta1) * dWi
	mom.Vdb[i] = beta1 * mom.Vdb[i] + (1.f0-beta1) * dbi
	mom.SdW[i] = beta2 * mom.SdW[i] + (1.f0-beta2) * dWi.^2
	mom.Sdb[i] = beta2 * mom.Sdb[i] + (1.f0-beta2) * dbi.^2
	state.W[i] -= (alpha * mom.VdW[i] / (1-beta1^t)) ./
		(sqrt.(mom.SdW[i] / (1-beta2^t) + 10f-8))
	state.b[i] -= (alpha * mom.Vdb[i] / (1-beta1^t)) ./
		(sqrt.(mom.Sdb[i] / (1-beta2^t) + 10f-8))
end

function batchtrain!(
	state	:: NNState,
	input	:: M32,
	output	:: M32,
	params	:: NNParams,
	interm	:: NNInterm,
	mom		:: NNMom,
	)
	alpha = params.alpha
	beta1 = params.beta1
	beta2 = params.beta2
	niter = params.niter
	keep = params.keep_prob
	costerr = params.costerr
	cost = 0.f0

	for i = 1:niter
		fwprop!(state, input, interm, keep)
		cost = fcost(interm.A[end], output)
		ewabkprop!(state, output, interm, mom, alpha, beta1)
		if (i-1)%1000 == 0
			@printf("cost : %f progress : %.1f%%\n", cost, (i-1)/niter*100)
		end
		if cost < costerr
			break
		end
	end
	@printf("cost : %f progress : 100.0%%\n", cost)
end

function minibatchtrain!(
	state	:: NNState,
	input	:: M32,
	output	:: M32,
	params	:: NNParams,
	interm	:: NNInterm,
	mom		:: NNMom,
	batchsz	:: Int64,
	)
	i = 1
	tot = ceil(Int64, size(input, 2) / batchsz)
	for idx in Iterators.partition(1:size(input, 2), batchsz)
		@printf("batch %d/%d\n", i, tot)
		batchtrain!(state, input[:, idx], output[:, idx], params, interm)
		i += 1
	end
end


function ppgdataset(
	normalize=true,
	ptst = 0.2,
	)
	dirs =
		string(ENV["HOME"], "/PPG/buttercup/"),
		string(ENV["HOME"], "/PPG/blossom/"),
		string(ENV["HOME"], "/PPG/bubbles/")

	fns = readdir.(dirs)
	m = sum(first.(size.(fns)))
	input, output = zeros(Float32, 64^2 * 3, m), zeros(Float32, 3, m)

	i = 1
	for p = 1:size(dirs, 1)
		for fn in fns[p]
			output[:, i] = [p == 1, p == 2, p == 3]
			input[:, i] = imgvec(load(string(dirs[p],fn)))
			i += 1
		end
	end
	idx = shuffle(1:m)
	input[:, :] = input[:, idx]
	output[:, :] = output[:, idx]
	mu = zeros(Float32, 64^2 * 3, 1)
	s2 = ones(Float32, 64^2 * 3, 1)

	ntst = floor(Int64, m * ptst)
	ndev = m - ntst
	idev, itst = input[:, 1:ndev], input[:, ndev+1:end]
	odev, otst = output[:, 1:ndev], output[:, ndev+1:end]
	if normalize
		mu = sum(idev, 2)/size(idev, 2)
		idev .-= mu
		s2 = sum(idev.^2, 2)/size(idev, 2)
		idev ./= s2
	end
	return (idev, itst, odev, otst, NNStat(mu, s2))
end

function testmodel(
	input	:: M32,
	output	:: M32,
	params	:: NNParams,
	state	:: NNState,
	stat	:: NNStat,
	)
	snormalize!(input, stat)
	interm = NNInterm(params.layer_size)
	fwprop!(state, input, interm)
	pred = findmax(interm.A[end])
	err = norm(interm.A[end] - output) / prod(size(output))
	@printf("error of test sample : %f%%\n", err*100)
end

function ppgclassify(
	fname	:: String,
	params	:: NNParams,
	state	:: NNState,
	stat	:: NNStat,
	)
	iid = imgresize(fname)
	ivd = imgvec(iid)
	imd = zeros(Float32, 64^2*3, 1)
	imd[:, 1] = ivd
	snormalize!(imd, stat)
	interm = NNInterm(params.layer_size)
	fwprop!(state, imd, interm)
	pred = findmax(interm.A[end])
	names = "buttercup", "blossom", "bubbles"
	println(names[pred[2]], " with p : ", pred[1])
end

params = NNParams(0.01, 0.9, 0.999, 5000, 0, [12288, 64, 64, 8, 3], 0.85f0)
