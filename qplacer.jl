import Base.parse

@enum Cut CutVert CutHorz
Surf{T} = Tuple{T, T, T, T}
Mat = SparseMatrixCSC{Float64, Int64}
Vec{T} = Array{T, 1}
Vec2{T} = Vec{Vec{T}}

struct Input
	gatenetlst :: Vec2{Int64}
	netgatelst :: Vec2{Int64}
	padnetlst
	ngates
	nnets
	npads
end

readint(f, c) = parse(Int64, readuntil(f, c))
spidx(m) = ziptupvec(findn(m))
draw(points) = pipepoints(`gnuplot -p -e "plot '<cat'"`, points)
ziptupvec(v) = ((x, y) = v; zip(x, y))
makepads(size) = Vec{Int64}(size), Vec{Float64}(size), Vec{Float64}(size)

function writesol(fname, pts)
	open(fname, "w") do f
		for (p, x, y) in zip(Iterators.countfrom(1), pts[1], pts[2])
			@printf(f, "%d %.8f %.8f\n", p, x, y)
		end
	end
end

function pipepoints(cmd, points)
	open(cmd, "w", STDOUT) do io
		for (x, y) in points
			println(io, x, " ", y)
		end
	end
end

function makelist(n)
	res = Vec2{Int64}(n)
	for i = 1:n
		res[i] = Vec{Int64}(0)
	end
	return res
end

function parsegatenetlist(f)
	ngates, nnets = readint(f, " "), readint(f, "\n")
	gatelist = makelist(ngates)
	for i = 1:ngates
		ngate, ngnets = readint(f, " "), readint(f, " ")
		nets = Array{Int64, 1}(ngnets)
		for j = 1:ngnets
			net = readint(f, j != ngnets ? " " : "\n")
			nets[j] = net
		end
		gatelist[i] = nets
	end
	return (gatelist, nnets)
end

function parsepadnetlist(f)
	npads = readint(f, "\n")
	pads = makepads(npads)
	for i = 1:npads
		pad, net, x, y = readint(f, " "), readint(f, " "), readint(f, " "),
			readint(f, "\n")
		pads[1][i] = net
		pads[2][i] = x
		pads[3][i] = y
	end
	return (pads, npads)
end

function parseinput(filename)
	open(filename) do f
		(gatenetlst, nnets) = parsegatenetlist(f)
		(padnetlst, npads) = parsepadnetlist(f)
		(netgatelst, ngates) = gatetonetlist(gatenetlst, padnetlst, nnets)
		return Input(gatenetlst, netgatelst, padnetlst, ngates, nnets, npads)
	end
end

function gatetonetlist(gatenetlst, padnetlst, nnets)
	ngates = size(gatenetlst, 1)
	npads = size(padnetlst[1], 1)
	netgatelst = makelist(nnets)

	for i = 1:ngates
		for net in gatenetlst[i]
			push!(netgatelst[net], i)
		end
	end
	for i = 1:npads
		push!(netgatelst[padnetlst[1][i]], i + ngates)
	end
	return (netgatelst, ngates)
end

function wmatrix(netgatelst, ngates, npads)
	W = spzeros(ngates + npads, ngates + npads)
	for net in netgatelst
		k = size(net, 1)

		w = 1.0 / (k - 1)
		for i = 1:size(net, 1)
			for j = (i+1):size(net, 1)
				fstnode, sndnode = net[i], net[j]
				W[fstnode, sndnode] = w
				W[sndnode, fstnode] = w
			end
		end
	end
	return W
end

function hord(p1, p2)
	x1, y1 = p1
	x2, y2 = p2
	if x1 < x2 return true
	elseif x2 < x1 return false
	else return y1 < y2
	end
end

function vord(p1, p2)
	x1, y1 = p1
	x2, y2 = p2
	if y1 < y2 return true
	elseif y2 < y1 return false
	else return x1 < x2
	end
end

function cutprio(surf :: Surf{Float64})
	(xmin, xmax, ymin, ymax) = surf
	if ymax - ymin > xmax - xmin                                           
		return CutHorz
	end
	return CutVert
end

function cutsurf(surf :: Surf{Float64}, cut :: Cut)
	(xmin, xmax, ymin, ymax) = surf
	if cut == CutHorz
		s1 = (xmin, xmax, ymin, ymax/2)
		s2 = (xmin, xmax, ymax/2, ymax)
		return (s1, s2)
	else
		s1 = (xmin, xmax/2, ymin, ymax)
		s2 = (xmax/2, xmax, ymin, ymax)
		return (s1, s2)
	end
end

function solveord(
		W :: Mat,
		ngates :: Int64,
		padx :: Vec{Float64},
		pady :: Vec{Float64},
		surf :: Surf{Float64},
		ncut :: Int64
	)
	ngatenpads = size(W, 2)

	A = W[1:ngates, 1:ngates]
	A = spdiagm(sum(W[1:ngates, :], 2)[:]) - A
	bx = W[1:ngates, ngates + 1:end] * padx
	by = W[1:ngates, ngates + 1:end] * pady

	L = cholfact(A)
	x, y = L \ bx, L \ by

	if ncut == 0
		return (x, y)
	end
	p = collect(zip(x, y))
	typecut = cutprio(surf)
	pos = sortperm(p, lt = typecut == CutVert ? hord : vord)

	fstngates = div(ngates, 2)
	sndngates = ngates - fstngates
	fstpos, sndpos = pos[1:fstngates], pos[fstngates + 1:end]

	fstpadx, fstpady = [x[sndpos]; padx], [y[sndpos]; pady]
	sndpadx, sndpady = [x[fstpos]; padx], [y[fstpos]; pady]

	fstW = W[fstpos, [fstpos; sndpos; ngates + 1:ngatenpads]]
	sndW = W[sndpos, [sndpos; fstpos; ngates + 1:ngatenpads]]

	fstsurf, sndsurf = cutsurf(surf, typecut)

	(fstxmin, fstxmax, fstymin, fstymax) = fstsurf
	(sndxmin, sndxmax, sndymin, sndymax) = sndsurf
	if typecut == CutVert
		fstpadx = clamp.(fstpadx, fstxmin, fstxmax)
		sndpadx = clamp.(sndpadx, sndxmin, sndxmax)
	else
		fstpady = clamp.(fstpady, fstymin, fstymax)
		sndpady = clamp.(sndpady, sndymin, sndymax)
	end
	fstx, fsty = solveord(fstW, fstngates, fstpadx, fstpady, fstsurf, ncut - 1)
	sndx, sndy = solveord(sndW, sndngates, sndpadx, sndpady, sndsurf, ncut - 1)

	x[fstpos] = fstx
	x[sndpos] = sndx
	y[fstpos] = fsty
	y[sndpos] = sndy
	return (x, y)
end

function solve(
		W :: Mat,
		ngates :: Int64,
		padx :: Vec{Float64},
		pady :: Vec{Float64},
		surf :: Surf{Float64},
		ncut :: Int64
	)
	ngatenpads = size(W, 2)

	A = W[1:ngates, 1:ngates]
	A = spdiagm(sum(W[1:ngates, :], 2)[:]) - A
	bx = W[1:ngates, ngates + 1:end] * padx
	by = W[1:ngates, ngates + 1:end] * pady

	L = cholfact(A)
	x, y = L \ bx, L \ by

	p = collect(zip(x, y))
	typecut = cutprio(surf)
	pos = sortperm(p, lt = typecut == CutVert ? hord : vord)

	if ncut == 0
		return (x[pos], y[pos])
	end
	fstngates = div(ngates, 2)
	sndngates = ngates - fstngates
	fstpos, sndpos = pos[1:fstngates], pos[fstngates + 1:end]

	fstpadx, fstpady = [x[sndpos]; padx], [y[sndpos]; pady]
	sndpadx, sndpady = [x[fstpos]; padx], [y[fstpos]; pady]

	fstW = W[fstpos, [fstpos; sndpos; ngates + 1:ngatenpads]]
	sndW = W[sndpos, [sndpos; fstpos; ngates + 1:ngatenpads]]

	fstsurf, sndsurf = cutsurf(surf, typecut)
	(fstxmin, fstxmax, fstymin, fstymax) = fstsurf
	(sndxmin, sndxmax, sndymin, sndymax) = sndsurf
	if typecut == CutVert
		fstpadx = clamp.(fstpadx, fstxmin, fstxmax)
		sndpadx = clamp.(sndpadx, sndxmin, sndxmax)
	else
		fstpady = clamp.(fstpady, fstymin, fstymax)
		sndpady = clamp.(sndpady, sndymin, sndymax)
	end
	fstx, fsty = solve(fstW, fstngates, fstpadx, fstpady, fstsurf, ncut - 1)
	sndx, sndy = solve(sndW, sndngates, sndpadx, sndpady, sndsurf, ncut - 1)

	return ([fstx; sndx], [fsty; sndy])
end

for fname in ARGS
	input = parseinput(fname)
	W = wmatrix(input.netgatelst, input.ngates, input.npads)
	padx = input.padnetlst[2]
	pady = input.padnetlst[3]
	xmin, xmax = extrema(input.padnetlst[2])
	ymin, ymax = extrema(input.padnetlst[3])
	surf = (xmin, xmax, ymin, ymax)
	ncut = 1

	res = solve(W, input.ngates, padx, pady, surf, ncut)
	fileout = match(r".*/(.*)", fname)[1]
	writesol("../../Downloads/VLSI/" * fileout, res)
end
