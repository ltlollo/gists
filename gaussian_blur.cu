#include "reference_calc.cpp"
#include "utils.h"

// Udacity CUDA P2 homework

__global__
void gaussian_blur(const unsigned char* const inputChannel,
                   unsigned char* const outputChannel,
                   int numRows, int numCols,
                   const float* const filter, const int filterWidth)
{
  extern __shared__ float buf[];
  
  const int2 bufDim = make_int2(blockDim.x + filterWidth, blockDim.y + filterWidth);
  const int2 thread_2D_pos = make_int2( blockIdx.x * blockDim.x + threadIdx.x,
                                        blockIdx.y * blockDim.y + threadIdx.y);

  int y, x, i = 0, j = 0;
  
  y = min(max(thread_2D_pos.y + i - filterWidth/2, 0), numRows-1);
  x = min(max(thread_2D_pos.x + j - filterWidth/2, 0), numCols-1);
  buf[(threadIdx.y + i) * bufDim.x + threadIdx.x + j] = inputChannel[y * numCols + x];
  i = filterWidth;
  j = 0;
  y = min(max(thread_2D_pos.y + i - filterWidth/2, 0), numRows-1);
  x = min(max(thread_2D_pos.x + j - filterWidth/2, 0), numCols-1);
  buf[(threadIdx.y + i) * bufDim.x + threadIdx.x + j] = inputChannel[y * numCols + x];
  i = 0;
  j = filterWidth;
  y = min(max(thread_2D_pos.y + i - filterWidth/2, 0), numRows-1);
  x = min(max(thread_2D_pos.x + j - filterWidth/2, 0), numCols-1);
  buf[(threadIdx.y + i) * bufDim.x + threadIdx.x + j] = inputChannel[y * numCols + x];
  i = filterWidth;
  j = filterWidth;
  y = min(max(thread_2D_pos.y + i - filterWidth/2, 0), numRows-1);
  x = min(max(thread_2D_pos.x + j - filterWidth/2, 0), numCols-1);
  buf[(threadIdx.y + i) * bufDim.x + threadIdx.x + j] = inputChannel[y * numCols + x];
  
  __syncthreads();

  float m = 0.0f;
  for (int i = 0; i < filterWidth; i++)
	for (int j = 0; j < filterWidth; j++)
	  m += buf[(threadIdx.y + i) * bufDim.x + threadIdx.x + j] * filter[i * filterWidth + j];
  
  if (thread_2D_pos.y < numRows && thread_2D_pos.x < numCols)
    outputChannel[thread_2D_pos.y * numCols + thread_2D_pos.x] = m;
}

__global__
void separateChannels(const uchar4* const inputImageRGBA,
                      int numRows,
                      int numCols,
                      unsigned char* const redChannel,
                      unsigned char* const greenChannel,
                      unsigned char* const blueChannel)
{  
  const int2 thread_2D_pos = make_int2( blockIdx.x * blockDim.x + threadIdx.x,
                                        blockIdx.y * blockDim.y + threadIdx.y);

  const int thread_1D_pos = thread_2D_pos.y * numCols + thread_2D_pos.x;

  //make sure we don't try and access memory outside the image
  //by having any threads mapped there return early
  if (thread_2D_pos.x >= numCols || thread_2D_pos.y >= numRows)
    return;

  redChannel[thread_1D_pos] = inputImageRGBA[thread_1D_pos].x;
  greenChannel[thread_1D_pos] = inputImageRGBA[thread_1D_pos].y;
  blueChannel[thread_1D_pos] = inputImageRGBA[thread_1D_pos].z;
}

__global__
void recombineChannels(const unsigned char* const redChannel,
                       const unsigned char* const greenChannel,
                       const unsigned char* const blueChannel,
                       uchar4* const outputImageRGBA,
                       int numRows,
                       int numCols)
{
  const int2 thread_2D_pos = make_int2( blockIdx.x * blockDim.x + threadIdx.x,
                                        blockIdx.y * blockDim.y + threadIdx.y);

  const int thread_1D_pos = thread_2D_pos.y * numCols + thread_2D_pos.x;

  if (thread_2D_pos.x >= numCols || thread_2D_pos.y >= numRows)
    return;

  unsigned char red   = redChannel[thread_1D_pos];
  unsigned char green = greenChannel[thread_1D_pos];
  unsigned char blue  = blueChannel[thread_1D_pos];

  //Alpha should be 255 for no transparency
  uchar4 outputPixel = make_uchar4(red, green, blue, 255);

  outputImageRGBA[thread_1D_pos] = outputPixel;
}

unsigned char *d_red, *d_green, *d_blue;
float         *d_filter;

void allocateMemoryAndCopyToGPU(const size_t numRowsImage, const size_t numColsImage,
                                const float* const h_filter, const size_t filterWidth)
{
  checkCudaErrors(cudaMalloc(&d_red,   sizeof(unsigned char) * numRowsImage * numColsImage));
  checkCudaErrors(cudaMalloc(&d_green, sizeof(unsigned char) * numRowsImage * numColsImage));
  checkCudaErrors(cudaMalloc(&d_blue,  sizeof(unsigned char) * numRowsImage * numColsImage));
  checkCudaErrors(cudaMalloc(&d_filter,  sizeof(float) * filterWidth * filterWidth));

  cudaMemcpy(d_filter, h_filter, sizeof(float) * filterWidth * filterWidth, cudaMemcpyHostToDevice);
}

void your_gaussian_blur(const uchar4 * const h_inputImageRGBA, uchar4 * const d_inputImageRGBA,
                        uchar4* const d_outputImageRGBA, const size_t numRows, const size_t numCols,
                        unsigned char *d_redBlurred, 
                        unsigned char *d_greenBlurred, 
                        unsigned char *d_blueBlurred,
                        const int filterWidth)
{
  const int nt = 16;
  const dim3 blockSize(nt, nt, 1);
  const dim3 gridSize((numCols)/nt+1, (numRows)/nt+1, 1);
  size_t shmem = (nt + filterWidth) * (nt * filterWidth);

  separateChannels<<<gridSize, blockSize>>>(d_inputImageRGBA,
                      numRows,
                      numCols,
                      d_red,
                      d_green,
                      d_blue);

  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  gaussian_blur<<<gridSize, blockSize, shmem>>>(d_red,
                   d_redBlurred,
                   numRows, numCols,
                   d_filter, filterWidth);
  gaussian_blur<<<gridSize, blockSize, shmem>>>(d_green,
                   d_greenBlurred,
                   numRows, numCols,
                   d_filter, filterWidth);
  gaussian_blur<<<gridSize, blockSize, shmem>>>(d_blue,
                   d_blueBlurred,
                   numRows, numCols,
                   d_filter, filterWidth);
  

  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  recombineChannels<<<gridSize, blockSize>>>(d_redBlurred,
                                             d_greenBlurred,
                                             d_blueBlurred,
                                             d_outputImageRGBA,
                                             numRows,
                                             numCols);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
}

void cleanup() {
  checkCudaErrors(cudaFree(d_red));
  checkCudaErrors(cudaFree(d_green));
  checkCudaErrors(cudaFree(d_blue));
  checkCudaErrors(cudaFree(d_filter));
}
