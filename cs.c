/*
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] = [0]
[0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0] = [1]
[1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1] = [2]
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] = [3]
[0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0] = [4]
[0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0] = [5]
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] = [6]
[0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0] = [7]
[1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1] = [8]
----

[ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0] = [0]
[ 0,  2,  0,  4,  0,  0,  0,  8,  0, 10,  0,  0,  0, 14,  0, 16,  0,  0,  0] = [1]
[ 1,  0,  3,  0,  0,  0,  7,  0,  9,  0,  0,  0, 13,  0, 15,  0,  0,  0, 19] = [2]
[ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0] = [3]
[ 0,  2,  0,  0,  0,  6,  0,  8,  0,  0,  0, 12,  0, 14,  0,  0,  0, 18,  0] = [4]
[ 0,  0,  3,  0,  5,  0,  0,  0,  9,  0, 11,  0,  0,  0, 15,  0, 17,  0,  0] = [5]
[ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0] = [6]
[ 0,  0,  0,  4,  0,  6,  0,  0,  0, 10,  0, 12,  0,  0,  0, 16,  0, 18,  0] = [7]
[ 1,  0,  0,  0,  5,  0,  7,  0,  0,  0, 11,  0, 13,  0,  0,  0, 17,  0, 19] = [8]

[z9] = exp +2/+4    growth
[1] = 2 +2          1.3333
[2] = 1 +2          0.6666
[4] = 2 +4          1.3333
[5] = 3 +2          2.6666
[7] = 4 +2          5.3333
[8] = 1 +4          0.6666

avg +height = 2.1666, growth = 2.0000


node exp +2/+4 height

gen():
    newnode = (node * 2 ^ exp - 1)/3
    node exp +2/+4 height <- node (exp+2/4) +4/+2 height
    newnode z9[newnode%9].exp z9[..].+2/+4 (height+exp+1)

prune the tree of ((node * 2*x) - 1) / 3 multiples of 3

1 4 +4 0 <
---

1 4 +4 0
5 3 +2 5





score(node, height, exp):
    height + node * 2 ^ exp / (height + 1)
    or height + node * 2 ^ (exp-1) / (height + 1)
 *
 * */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

typedef unsigned long u64;
typedef unsigned u32;
typedef unsigned char u8;
typedef float f32;

struct Value {
    u64 node;
    u8 exp;
    u8 add;
    u64 height;
    f32 score;
};

struct Arr {
    u64 size;
    u64 alloc;
    struct Value *data;
};

static struct Z9 {
    u8 exp;
    u8 add;
} z9[] = {
    [1] = { 2, +2 },
    [2] = { 1, +2 },
    [4] = { 2, +4 },
    [5] = { 3, +2 },
    [7] = { 4, +2 },
    [8] = { 1, +4 },
};

f32
_score(struct Value *it) {
    return logf((f32)(it->node) / (it->height + 1.f) * logf(it->height + 1.f))
         - logf((f32)(it->node) * (1ull<<(it->exp)) / (it->height + 1.f));
}
f32
score(struct Value *it) {
    return (f32)(it->height) / (logf(it->node) + 1.f) - it->exp * (logf(it->node) / 100 + 1);
         //- logf((f32)(it->node) * (1ull<<(it->exp)) / (it->height + 1.f));
         ;
}


u64
count(u64 x) {
    u64 res = 0;
    while (x != 1) {
        x = x * 3 + 1;
        res++;
        while (x % 2 == 0) {
            res++;
            x /= 2;
        }
    }
    return res;
}

struct Value *
find_insertion(struct Value *arr, u64 size, f32 score) {
    struct Value *beg = arr;
    struct Value *end = arr + size;

    while (end - beg > 0) {
        struct Value *middle = (end - beg) / 2 + beg;

        if (middle->score < score) {
            beg = middle + 1;
        } else {
            end = middle;
        }
    }
    return beg;
}

int
grow(struct Arr *arr, u64 pos) {
    assert(pos < arr->size);

    struct Value it = arr->data[pos];
    u64 newnode;
    int overflow = __builtin_umull_overflow(it.node, (1ull << it.exp), &newnode);

    newnode = (newnode - 1) / 3;


    struct Value new = {
        .node = newnode,
        .exp = z9[newnode % 9].exp,
        .add = z9[newnode % 9].add,
        .height = it.height + it.exp + 1,
    };
    it.exp += it.add;
    it.add = it.add == 4 ? +2 : +4;

    //assert(new.height == count(new.node));

    new.score = score(&new);
    it.score  = score(&it);

    if (arr->size + 1 > arr->alloc) {
        u64 alloc = (arr->alloc + 1) * 2;
        struct Value *data = realloc(arr->data, alloc * sizeof(struct Value));
        if (data == NULL) {
            return -1;
        }
        arr->data = data;
        arr->alloc = alloc;
    }
    struct Value *itp = find_insertion(arr->data, arr->size, it.score);

    if (overflow) itp = arr->data;
    //assert(itp - arr->data <= pos);
    if (itp - arr->data > pos) {
        itp = arr->data + pos;
    }
    struct Value *exp = arr->data + pos;
    memmove(itp + 1, itp, (exp - itp) * sizeof(struct Value));
    *itp = it;
    
    if (!overflow) {
        struct Value *newp = find_insertion(arr->data, arr->size, new.score);
        struct Value *endp = arr->data + arr->size;

        memmove(newp + 1, newp, (endp - newp) * sizeof(struct Value));
        *newp = new;

        arr->size += 1;
    }
    

    if (arr->size > 5000000000 / 100000) {
        struct Value *beg = arr->data + 2500000000 / 100000;
        struct Value *end = arr->data + arr->size;
        memmove(arr->data, beg, (end - beg) * sizeof(struct Value));
        arr->size = end - beg;
    }
    return 0;
}

void
printvalue(struct Value *v) {
    printf("% 10ld % 2d % 2d %10ld %f\n", v->node, v->exp, v->add, v->height, v->score);
}

void
printarr(struct Arr *arr) {
    for (u64 i = 0; i < arr->size; i++) {
        printvalue(arr->data + i);
    }
    printf("SIZE: %ld\n", arr->size);
}


int
main() {
    struct Arr arr = {
        .data = malloc(sizeof (struct Value)),
        .alloc = 1,
        .size = 1,
    };
    struct Value *first = arr.data;
    *first = (struct Value) {
        .node = 1,
        .exp = 4,
        .add = 4,
        .height = 0,
    };
    first->score = score(first);

    for (u64 i = 0, j = 0;; i++) {
        grow(&arr, arr.size - 1);
        struct Value * it = arr.data + arr.size - 1;

        if (it->height > 900) {
            printvalue(arr.data + arr.size - 1);
        }
    }
    return 0;    
}
