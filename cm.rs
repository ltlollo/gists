use std::collections::HashSet;

/* x * 2 ^ pos - 1 is divisible by 3 but not 9
 * 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... = [0]
 * 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, ... = [1]
 * 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, ... = [2]
 * 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... = [3]
 * 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, ... = [4]
 * 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, ... = [5]
 * 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... = [6]
 * 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, ... = [7]
 * 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, ... = [8]
 * eg 5 = 2 mod 3 -> 5 * 2 ^ {1, 3, 7, 9, ...} - 1 %3 = 0, %9 != 0
 */

fn factor(ni : u32) -> Vec<u32> {
    let mut res = vec![];
    let mut i = 2;
    let mut n = ni;
    while i < (n as f32).sqrt() as u32 + 2 {
        while n % i == 0 {
            n /= i;
            res.push(i)
        }
        i += 1
    }
    if n != 1 {
        res.push(n)
    }
    res
}

fn compute(xi: u32) -> u32 {
    let mut x = xi * 3 + 1;

    while x % 8 == 0 {
        x /= 8;
    }
    while x % 4 == 0 {
        x /= 4;
    }
    while x % 2 == 0 {
        x /= 2;
    }
    x
}

const H1 : u32 = 317;
const H2 : u32 = 643;
const H3 : u32 = 997;

fn gencolor(n: u32, ls: Vec<u32>) -> String {
    let mut s : String = n.to_string();
    if ls.len() != 1 {
        s += " [label=<<table border=\"0\" cellborder=\"0\" cellspacing=\"0\">";
    } else {
        s += " [label=<<table border=\"8\" color=\"#3333ff\" cellborder=\"0\" cellspacing=\"0\">";
    }
    for i in ls {
        let (mut r, mut g, mut b) = ((i * H1) & 0xff, (i * H2) & 0xff, (i * H3) & 0xff);
        if r  / 4 + g / 3 + b / 6 < 80 {
            r = 255 - r;
            g = 255 - g;
            b = 255 - b;
        }
        s += &format!("<tr><td bgcolor=\"#{:02x}{:02x}{:02x}\">{}</td></tr>", r, g, b, i);
    }
    if n % 3 != 0 {
        s += &format!("<tr><td>{}</td></tr>", n % 9);
    }
    s += "</table>>]";
    s
}

fn main() {
    let mut col  = HashSet::new();
    let mut comp = HashSet::new();

    println!("digraph c {{\nnode [shape=plaintext]\n{}", gencolor(1, vec![1]));
    col.insert(1);
    comp.insert(1);

    for i in 0..1500 {
        let mut x = 2 * i + 1;
        while !comp.contains(&x) {
            let y = compute(x);
            let mut f = factor(y);
            if !col.contains(&y) {
                println!("{}", gencolor(y, f));
                col.insert(y);
            }
            f = factor(x);
            println!("{}", gencolor(x, f));
            col.insert(x);
            if x > y {
                println!("{} -> {} [color=\"blue\"]", x, y);
            } else {
                println!("{} -> {} [color=\"red\"]", x, y);
            }
            comp.insert(x);
            x = y
        }
    }
    println!("}}");
}
