#include <stdio.h>
#include <queue>
#include <memory>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#ifdef NDEBUG
#define assert(e) e
#else
#include <assert.h>
#endif

struct Cell {
	int cost : 12;
	unsigned reached : 1;
	unsigned pred : 3;
};

struct Net {
	int src_z;
	int src_x;
	int src_y;
	int tgt_z;
	int tgt_x;
	int tgt_y;
};

struct Front {
	int z;
	int x;
	int y;
	int cost;
	unsigned pred : 3;
};

struct Coord {
	int z;
	int x;
	int y;
};

enum DIREC {
	DIR_NONE = 0,
	DIR_N,
	DIR_S,
	DIR_E,
	DIR_W,
	DIR_U,
	DIR_D,
	DIR_END,
};

template<typename T> using Heap = std::priority_queue<T, std::vector<T>,
	std::greater<T>>;

bool operator>(const Front &f, const Front &s) {
	return f.cost > s.cost;
}

struct Problem {
	int x;
	int y;
	int bend_cost;
	int via_cost;
	int nnets;
	std::unique_ptr<Cell[]> cell_costs;
	std::unique_ptr<Net[]> nets;
	FILE *out;
};

Cell &cell_ref(int x, int y, int z, Problem &p) {
	return p.cell_costs[(y * p.x + x) * 2 + z];
}

Cell *cell_ptr(int z, int x, int y, Problem &p) {
	return &p.cell_costs[0] + (y * p.x + x) * 2 + z;
}


Problem parse_file(const char *prefix) {
	Problem info;
	static constexpr int nlayers = 2;
	std::string fname = prefix;
	std::string str;
	{
		std::ifstream f(fname + ".grid");
		assert(std::getline(f, str));
		std::istringstream iss(str);

		iss >> info.x >> info.y >> info.bend_cost >> info.via_cost;
		info.cell_costs = std::make_unique<Cell[]>(nlayers * info.x * info.y);
		for (int z = 0; z < nlayers; z++) {
			for (int y = 0; y < info.y; y++) {
				assert(std::getline(f, str));
				std::istringstream iss(str);
				int xyz_cost;
				for (int x = 0; x < info.x; x++) {
					iss >> xyz_cost;
					cell_ref(x, y, z, info) = Cell{xyz_cost, 0, 0};
				}
			}
		}
	}
	std::ifstream f(fname + ".nl");
	assert(std::getline(f, str));
	std::istringstream iss(str);

	iss >> info.nnets;
	info.nets = std::make_unique<Net[]>(info.nnets);

	int id;
	Net net;
	for (int i = 0; i < info.nnets; i++) {
		assert(std::getline(f, str));
		std::istringstream iss(str);
		
		iss >> id;
		iss >> net.src_z >> net.src_x >> net.src_y;
		iss >> net.tgt_z >> net.tgt_x >> net.tgt_y;

		assert(id <= info.nnets);
		net.src_z -= 1;
		net.tgt_z -= 1;
		info.nets[id - 1] = net;
	}
	info.out = fopen((fname).c_str(), "w");

	assert(info.out);
	return info;
}

bool is_cell(Front &c, int x, int y, int z) {
	return c.z == z && c.x == x && c.y == y;
}

void cleanup(Problem &p) {
	for (int i = 0; i < 2 * p.x * p.y; i++) {
		p.cell_costs[i].reached = 0;
		p.cell_costs[i].pred = DIR_NONE;
	}
}

int dir_nearsol(int cz, int cx, int cy, int tz, int tx, int ty) {
	if ((cz-tz)*(cz-tz)+(cx-tx)*(cx-tx)+(cy-ty)*(cy-ty) != 1) {
		return DIR_NONE;
	} else if (cx-tx == 1) {
		return DIR_W;
	} else if (cx-tx == -1) {
		return DIR_E;
	} else if (cy-ty == 1) {
		return DIR_S;
	} else if (cy-ty == -1) {
		return DIR_N;
	} else if (cz-tz == 1) {
		return DIR_D;
	} else {
		return DIR_U;
	}
}

int route_net(Problem &info, int netn) {
	auto net = info.nets[netn];

	int steps = 0;
	int total_cost;
	int via_cost = info.via_cost;
	int bend_cost;
	int sol_dir;
	Cell *n;
	Heap<Front> wf;

	cell_ref(net.src_x, net.src_y, net.src_z, info).cost = 0;
	cell_ref(net.src_x, net.src_y, net.src_z, info).reached = 1;
	cell_ref(net.tgt_x, net.tgt_y, net.tgt_z, info).cost = 0;
	wf.emplace(Front{ net.src_z, net.src_x, net.src_y, 0, DIR_NONE});

	if (net.src_z == net.tgt_z && net.src_x == net.tgt_x &&
		net.src_y == net.tgt_y) {
		return 0;
	}
	while (true) {
		if (wf.empty()) {
			return -1;
		}
		Front c = wf.top();
		sol_dir = dir_nearsol(c.z, c.x, c.y, net.tgt_z, net.tgt_x, net.tgt_y);

		if (sol_dir != DIR_NONE) {
			n = cell_ptr(net.tgt_z, net.tgt_x, net.tgt_y, info);
			n->pred = sol_dir;
			if (sol_dir == DIR_U || sol_dir == DIR_D) {
				total_cost = info.via_cost;
			} else if (c.pred != sol_dir) {
				total_cost = info.bend_cost;
			} else {
				total_cost = 0;
			}
			total_cost += c.cost + n->cost;
			break;
		}
		wf.pop();

		bend_cost = c.pred == DIR_N ? 0 : info.bend_cost;
		n = cell_ptr(c.z, c.x, c.y+1, info);
		if (c.y != info.y-1 && n->cost >= 0 && !n->reached) {
			wf.emplace(Front{ c.z, c.x, c.y+1, c.cost + n->cost + bend_cost, DIR_N });
			n->reached = 1;
			n->pred = DIR_N;
		}
		bend_cost = c.pred == DIR_S ? 0 : info.bend_cost;
		n = cell_ptr(c.z, c.x, c.y-1, info);
		if (c.y != 0 && n->cost >= 0 && !n->reached) {
			wf.emplace(Front{ c.z, c.x, c.y-1, c.cost + n->cost + bend_cost, DIR_S });
			n->reached = 1;
			n->pred = DIR_S;
		}
		bend_cost = c.pred == DIR_E ? 0 : info.bend_cost;
		n = cell_ptr(c.z, c.x+1, c.y, info);
		if (c.x != info.x-1 && n->cost >= 0 && !n->reached) {
			wf.emplace(Front{ c.z, c.x+1, c.y, c.cost + n->cost + bend_cost, DIR_E });
			n->reached = 1;
			n->pred = DIR_E;
		}
		bend_cost = c.pred == DIR_W ? 0 : info.bend_cost;
		n = cell_ptr(c.z, c.x-1, c.y, info);
		if (c.x != 0 && n->cost >= 0 && !n->reached) {
			wf.emplace(Front{ c.z, c.x-1, c.y, c.cost + n->cost + bend_cost, DIR_W });
			n->reached = 1;
			n->pred = DIR_W;
		}
		bend_cost = c.pred == DIR_U ? 0 : info.bend_cost;
		n = cell_ptr(c.z+1, c.x, c.y, info);
		if (c.z == 0 && n->cost >= 0 && !n->reached) {
			wf.emplace(Front{ c.z+1, c.x, c.y, c.cost + n->cost + via_cost, DIR_U });
			n->reached = 1;
			n->pred = DIR_U;
		}
		bend_cost = c.pred == DIR_D ? 0 : info.bend_cost;
		n = cell_ptr(c.z-1, c.x, c.y, info);
		if (c.z == 1 && n->cost >= 0 && !n->reached) {
			wf.emplace(Front{ c.z-1, c.x, c.y, c.cost + n->cost + via_cost, DIR_D });
			n->reached = 1;
			n->pred = DIR_D;
		}
		steps++;
	}

	Coord curr{ net.tgt_z, net.tgt_x, net.tgt_y };
	std::vector<Coord> path{curr};
	
	while (curr.z != net.src_z || curr.x != net.src_x || curr.y != net.src_y) {
		auto &curr_cell = cell_ref(curr.x, curr.y, curr.z, info);
		switch (curr_cell.pred) {
			case DIR_N: curr.y--; break;
			case DIR_S: curr.y++; break;
			case DIR_E: curr.x--; break;
			case DIR_W: curr.x++; break;
			case DIR_U: curr.z--; break;
			case DIR_D: curr.z++; break;
			default: assert(false);
		}
		assert(curr.z >= 0 && curr.z < 2 && curr.x >= 0 && curr.x < info.x &&
			   curr.y >= 0 && curr.y < info.y);
		path.push_back(curr);
		curr_cell.cost = -1;
	}
	for (auto it = path.rbegin(); it < path.rend(); it++) {
		if (it != path.rbegin() && (it-1)->z != it->z) {
			fprintf(info.out, "%d %d %d\n", 3, it->x, it->y);
		}
		fprintf(info.out, "%d %d %d\n", it->z + 1, it->x, it->y);
	}
	return 0;
}

int route(Problem &info) {
	fprintf(info.out, "%d\n", info.nnets);
	for (int i = 0; i < info.nnets; i++) {
		fprintf(info.out, "%d\n", i + 1);
		route_net(info, i);
		cleanup(info);
		fprintf(info.out, "0\n");
	}
	return 0;
}

int
main(int argc, char *argv[]) {
	if (argc - 1 < 1) {
		return 1;
	
	}
	auto info = parse_file(argv[1]);
	route(info);

	return 0;
}