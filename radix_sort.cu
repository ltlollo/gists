//Udacity HW 4 - Radix Sorting

#include "utils.h"
#include <thrust/host_vector.h>

extern __shared__ unsigned int ssbuf[];
 
#ifdef DEBUG
	#define cudaDbg() 		cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError())
#else
	#define cudaDbg()
#endif

__global__ void
unblocked_exclusive_sum_scan(unsigned int *out, const unsigned int *in) {
	unsigned int *ssbuf = out;
	int tid = threadIdx.x;
	unsigned int l, r;
	int j, i;

	ssbuf[tid] = in[tid];
	
	for (i = blockDim.x/2, j = 1; i; i /= 2, j *= 2) {
		__syncthreads();
		if (tid < i) {
			l = ssbuf[blockDim.x - 1 - tid * 2 * j - j],
			r = ssbuf[blockDim.x - 1 - tid * 2 * j];
		}
		__syncthreads();
		if (tid < i) {
			ssbuf[blockDim.x - 1 - tid * 2 * j] = l + r;
		}
	}
	__syncthreads();

	ssbuf[blockDim.x - 1] = 0;
	for (j = blockDim.x/2, i = 1; j; j /= 2, i *= 2) {
		__syncthreads();
		if (tid < i) {
			l = ssbuf[blockDim.x - 1 - tid * 2 * j - j],
			r = ssbuf[blockDim.x - 1 - tid * 2 * j];
		}
		__syncthreads();
		if (tid < i) {
			ssbuf[blockDim.x - 1 - tid * 2 * j] = l + r;
			ssbuf[blockDim.x - 1 - tid * 2 * j - j] = r;
		}
	}
	__syncthreads();
}

__global__ void
blocked_exclusive_sum_scan(unsigned int *out, const unsigned int *in, size_t size) {
	int tid = threadIdx.x;
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	unsigned int l, r;
	int j, i;

	if (gid < size) {
		ssbuf[tid] = in[gid];
	} else {
		ssbuf[tid] = 0;
	}
	for (i = blockDim.x/2, j = 1; i; i /= 2, j *= 2) {
		__syncthreads();
		if (tid < i) {
			l = ssbuf[blockDim.x - 1 - tid * 2 * j - j],
			r = ssbuf[blockDim.x - 1 - tid * 2 * j];
		}
		__syncthreads();
		if (tid < i) {
			ssbuf[blockDim.x - 1 - tid * 2 * j] = l + r;
		}
	}
	__syncthreads();

	ssbuf[blockDim.x - 1] = 0;
	for (j = blockDim.x/2, i = 1; j; j /= 2, i *= 2) {
		__syncthreads();
		if (tid < i) {
			l = ssbuf[blockDim.x - 1 - tid * 2 * j - j],
			r = ssbuf[blockDim.x - 1 - tid * 2 * j];
		}
		__syncthreads();
		if (tid < i) {
			ssbuf[blockDim.x - 1 - tid * 2 * j] = l + r;
			ssbuf[blockDim.x - 1 - tid * 2 * j - j] = r;
		}
	}
	__syncthreads();
	
	if (gid < size) {
		out[gid] = ssbuf[tid];
	}
}

__global__ void
blocked_scan_reduce(unsigned int *oo, const unsigned int *out, const unsigned int *in, size_t size) {
	int tid = threadIdx.x;
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	int i, j, k, l, r;

	ssbuf[tid] = blockDim.x * (tid + 1) - 1 < size ?
		out[blockDim.x * (tid + 1) - 1] + in[blockDim.x * (tid + 1) - 1] : 0;
	
	unsigned dim = blockDim.x;
	
	for (i = dim/2, j = 1, k = 0; i; i /= 2, j *= 2, k++) {
		__syncthreads();
		if (tid < i) {
			l = ssbuf[dim - 1 - tid * 2 * j - j],
			r = ssbuf[dim - 1 - tid * 2 * j];
		}
		__syncthreads();
		if (tid < i) {
			ssbuf[dim - 1 - tid * 2 * j] = l + r;
		}
	}
	__syncthreads();

	ssbuf[dim - 1] = 0;
	for (j = dim/2, i = 1, k = 0; j; j /= 2, i *= 2, k++) {
		__syncthreads();
		if (tid < i) {
			l = ssbuf[dim - 1 - tid * 2 * j - j],
			r = ssbuf[dim - 1 - tid * 2 * j];
		}
		__syncthreads();
		if (tid < i) {
			ssbuf[dim - 1 - tid * 2 * j] = l + r;
			ssbuf[dim - 1 - tid * 2 * j - j] = r;
		}
	}
	__syncthreads();
	
	unsigned int sum = ssbuf[blockIdx.x];
	
	if (gid < size) {
		oo[gid] = out[gid] + sum;
	}
}

unsigned int
exclusive_sum_scan(unsigned int *d_out, const unsigned int *d_in, size_t size, int blocked) {
	unsigned int *d_tmp;
	
	checkCudaErrors(cudaMalloc(&d_tmp, sizeof(*d_tmp) * size));
	
	unsigned int last_out, last_in;
	size_t nth, nbl, shmem;
	int i = 0;
	if (blocked == 0) {
		do {
			nth = 1 << i;
			i++;
		} while (nth < size);
		nbl = 1;
		shmem = nth * sizeof(unsigned int);
		unblocked_exclusive_sum_scan<<<1, size>>>(d_out, d_in);
	} else {		
		do {
			nth = 1<<i;
			nbl = size / nth + size % nth;
			i++;
		} while (nbl > nth);
		shmem = nth * sizeof (*d_out);
		blocked_exclusive_sum_scan<<<nbl, nth, shmem>>>(d_tmp, d_in, size);
cudaDbg();
		blocked_scan_reduce<<<nbl, nth, shmem>>>(d_out, d_tmp, d_in, size);
cudaDbg();
	}
	cudaFree(d_tmp);
	cudaMemcpy(&last_out, d_out + size - 1, sizeof (*d_out), cudaMemcpyDeviceToHost);
	cudaMemcpy(&last_in, d_in + size - 1, sizeof (*d_in), cudaMemcpyDeviceToHost);
	return last_out + last_in;
}

__global__ void
compact(unsigned int *d_out, const unsigned int *d_in, const unsigned int *d_scat,
		const unsigned int *d_mask, size_t size, unsigned int offset) {
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	
	if (gid >= size) {
		return;
	}	
	if (d_mask[gid]) {
		d_out[d_scat[gid] + offset] = d_in[gid];
	}
}
 
 __global__ void
 mask_gen(unsigned int *d_mask, const unsigned *d_in, size_t size, unsigned n, unsigned m, unsigned neg) {
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	
	if (gid >= size) {
		return;
	}
	d_mask[gid] = neg ? !((d_in[gid] >> n) & m) : ((d_in[gid] >> n) & m);
 }
 
void your_sort(unsigned int* const d_inputVals,
               unsigned int* const d_inputPos,
               unsigned int* const d_outputVals,
               unsigned int* const d_outputPos,
               const size_t numElems)
{

	unsigned int *d_mask, *d_sum, *d_tmpVals, *d_tmpPos;
	unsigned int nth = 1024;
	unsigned int nbl = numElems / nth + 1;
	unsigned int max;
	
	
	checkCudaErrors(cudaMalloc(&d_mask, sizeof(*d_mask) * numElems));
	checkCudaErrors(cudaMalloc(&d_sum, sizeof(*d_sum) * numElems));
	checkCudaErrors(cudaMalloc(&d_tmpVals, sizeof(*d_tmpVals) * numElems));
	checkCudaErrors(cudaMalloc(&d_tmpPos, sizeof(*d_tmpPos) * numElems));
	
	cudaMemcpy(d_tmpVals, d_inputVals, sizeof(*d_inputVals) * numElems, cudaMemcpyDeviceToDevice);
	cudaMemcpy(d_tmpPos, d_inputPos, sizeof(*d_inputPos) * numElems, cudaMemcpyDeviceToDevice);
	
	

	for (int i = 0; i < 32; i++) {
		cudaDbg();
		mask_gen<<<nbl, nth>>>(d_mask, d_tmpVals, numElems, i, 1, 1);
		cudaDbg();
		max = exclusive_sum_scan(d_sum, d_mask, numElems, 1);
		cudaDbg();				
		compact<<<nbl, nth>>>(d_outputVals, d_tmpVals, d_sum, d_mask, numElems, 0);
		compact<<<nbl, nth>>>(d_outputPos, d_tmpPos, d_sum, d_mask, numElems, 0);
		cudaDbg();
		
		mask_gen<<<nbl, nth>>>(d_mask, d_tmpVals, numElems, i, 1, 0);
		cudaDbg();
		exclusive_sum_scan(d_sum, d_mask, numElems, 1);
		cudaDbg();
		compact<<<nbl, nth>>>(d_outputVals, d_tmpVals, d_sum, d_mask, numElems, max);
		compact<<<nbl, nth>>>(d_outputPos, d_tmpPos, d_sum, d_mask, numElems, max);
		cudaDbg();
		
		cudaMemcpy(d_tmpVals, d_outputVals, sizeof(*d_tmpVals) * numElems, cudaMemcpyDeviceToDevice);
		cudaMemcpy(d_tmpPos, d_outputPos, sizeof(*d_tmpPos) * numElems, cudaMemcpyDeviceToDevice);
	}
}
